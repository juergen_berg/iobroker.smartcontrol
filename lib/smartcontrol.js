/* eslint-disable no-irregular-whitespace */
'use strict';

/**
 * SmartControl Library
 * * Note: You must call init() after assignment, since we use async/await for initialization, which does not work in constructor.
 * 
 * Generic JS functions - independent from adapter
 * 
 * 
 * @class Library
 * @description Library of various functions.
 * @author Mic-M <https://github.com/Mic-M/>
 * @license MIT License
 */
class Library {

    /**
     * Constructor.
     *
     * @param    {object}    adapter   - ioBroker adapter object
     * @param    {object}    globals   - global variables object from main.js
     */
    constructor(adapter, globals) {
        this._adapter = adapter;
        this._globals = globals;
        this._sunCalc = globals.suncalc;
        this._Schedule = globals.schedule;
        this.latitude = 0;
        this.longitude = 0;
        this._timers = {}; // Here we keep all timers
        this._timers.motion = {};
        this._timers.zoneOff = {}; // for option "offAfter" in Zones table
        this._schedules = {}; // Here we keep all schedules (node-schedule)
        this._onStateChangeTriggers = {};  // Keep triggerName and timestamp for checking for limitTriggerInterval to not switch more than x secs
        // Motion trigger should not set timeout if target device was turned on previously without a motion trigger.
        // Ref. https://forum.iobroker.net/post/433871 | https://forum.iobroker.net/post/437803
        /** @type {array} */
        this._motionTriggeredDevices = [];
    }

    /**
     * initialization
     */
    async init() {
        // Set geo coordinates from ioBroker admin main configuration.
        try {
            this.latitude = await this.asyncGetSystemConfig('latitude');
            this.longitude = await this.asyncGetSystemConfig('longitude');
            if (!this.latitude || !this.longitude) {
                this._adapter.log.warn('Latitude/Longitude is not defined in ioBroker main configuration, so you will not be able to use Astro functionality for schedules.');
            }                    
        } catch (error) {
            this.dumpError(`[init]`, error);
        }

    }

    /***
     *    ███████ ████████  █████  ████████ ███████      ██████ ██   ██  █████  ███    ██  ██████  ███████ ███████ 
     *    ██         ██    ██   ██    ██    ██          ██      ██   ██ ██   ██ ████   ██ ██       ██      ██      
     *    ███████    ██    ███████    ██    █████       ██      ███████ ███████ ██ ██  ██ ██   ███ █████   ███████ 
     *         ██    ██    ██   ██    ██    ██          ██      ██   ██ ██   ██ ██  ██ ██ ██    ██ ██           ██ 
     *    ███████    ██    ██   ██    ██    ███████      ██████ ██   ██ ██   ██ ██   ████  ██████  ███████ ███████ 
     *
     *    State Changes
     */

    /**
     * This is called once a foreign, target device changes of table tableTargetDevices: on/off states.
     * This is to set the smartcontrol.0.targetDevices states accordingly
     * 
     * @param {string}                            statePath    - State Path
     * @param {ioBroker.State | null | undefined} stateObject  - State object
     */
    async handleStateChangeTargetForeignTargets(statePath, stateObject) {

        try {

            if(!stateObject || !statePath) return;

            for (const lpRow of this._adapter.config.tableTargetDevices) {
                if (!lpRow.active) continue;  
                if (lpRow.onState != statePath && lpRow.onState != statePath) continue;

                if (lpRow.onState == statePath && lpRow.onValue == stateObject.val) {
                    await this._adapter.setStateAsync(`targetDevices.${lpRow.name}`, {val: true, ack: true });
                    this._adapter.log.debug(`State '${statePath}' changed to '${stateObject.val}' -> '${this._adapter.namespace}.targetDevices.${lpRow.name}' set to true.`);
                } else if (lpRow.offState == statePath && lpRow.offValue == stateObject.val) {
                    await this._adapter.setStateAsync(`targetDevices.${lpRow.name}`, {val: false, ack: true });
                    this._adapter.log.debug(`State '${statePath}' changed to '${stateObject.val}' -> '${this._adapter.namespace}.targetDevices.${lpRow.name}' set to false.`);
                }
            }

        } catch (error) {
            this.dumpError('[handleStateChangeTargetForeignTargets]', error);
            return;
        }        

    }


    /**
     * This is called once an adapter target device state changes like 'smartcontrol.0.targetDevices.BathroomRadio'
     * This is to set the tableTargetDevices states accordingly.
     * 
     * @param {string}                            statePath    - State Path
     * @param {ioBroker.State | null | undefined} stateObject  - State object
     */
    async handleStateChangeTargetDevices(statePath, stateObject) {

        try {

            if(!stateObject || !statePath) return;
            let targetDevicesRow = {};
            for (const lpRow of this._adapter.config.tableTargetDevices) {
                if (!lpRow.active) continue;  
                if (`${this._adapter.namespace}.targetDevices.${lpRow.name}` == statePath) {
                    targetDevicesRow = lpRow;
                    break;
                }
            }
            if(this.isLikeEmpty(targetDevicesRow)) throw (`Table 'Target Devices': No row found for state path ${statePath}`);

            // Set target states.
            // Note: Verification of the state value and conversion as needed was performed already by asyncVerifyConfig()
            const w = (stateObject.val) ? 'on' : 'off';
            await this._adapter.setForeignStateAsync(targetDevicesRow[w+'State'], {val: targetDevicesRow[w+'Value'], ack: false });

            // confirm by ack:true
            await this._adapter.setStateAsync(statePath, {val: stateObject.val, ack: true });
            
        } catch (error) {
            this.dumpError('[handleStateChangeTargetDevices]', error);
            return;
        }        

    }



    /**
     * Called once an adapter state changes like 'smartcontrol.0.options.Zones.Hallway.active'
     * 
     * @param {string}                            statePath    - State Path
     * @param {ioBroker.State | null | undefined} stateObject  - State object
     */
    async handleStateChangeOptionsActive(statePath, stateObject) {

        try {

            if(!stateObject || !statePath) return;

            // {name:'Hallway', index:2, table:'tableZones', field:'active', row:{.....} }
            const optionObj = await this.asyncGetOptionForOptionStatePath(statePath);

            // Check if new value != old value
            if (optionObj.row[optionObj.field] == stateObject.val) {
                this._adapter.log.info(`Smart Control Adapter State '${statePath}' changed to '${stateObject.val}', but is equal to old state val, so no action at this point.`);
                return;
            }

            // Info
            this.logExtendedInfo(`Smart Control Adapter State '${statePath}' changed to '${stateObject.val}'.`);

            // Acknowledge State Change
            await this._adapter.setStateAsync(statePath, {val:stateObject.val, ack: true});

            // Set config change into adapter configuration.
            // This will also restart the adapter instance by intention.
            // Restart is required since an activation or deactivation of a table row has multiple effects.
            this._adapter.log.info(`State change of '${statePath}' to '${stateObject.val}' now executes an adapter instance restart to put the change into effect.`);
            const resultObject = await this._adapter.getForeignObjectAsync(`system.adapter.${this._adapter.namespace}`);
            if (resultObject) {
                resultObject.native[optionObj.table][optionObj.index][optionObj.field] = stateObject.val;
                await this._adapter.setForeignObjectAsync(`system.adapter.${this._adapter.namespace}`, resultObject);
                return;
            } else {
                throw('getForeignObjectAsync(): No object provided from function.');
            }

        } catch (error) {
            this.dumpError('[handleStateChangeOptionsActive()]', error);
            return;
        }        

    }

    /**
     * Get the option for a given state option
     * 
     * @param {string} statePath  like 'smartcontrol.0.options.Zones.Hallway.active'
     *                              or 'smartcontrol.0.options.Zones.Flur EG.Lichter.Wandlicht 1.active'
     * @return {Promise<object>}    { 
     *                                name:'Hallway',
     *                                index:2, // index of the table
     *                                table:'tableZones',
     *                                tableName:'Zones 
     *                                field:'active'
     *                                row:{}   // The full row
     *                               }
     */
    async asyncGetOptionForOptionStatePath(statePath) {

        try {
            
            if ( statePath.startsWith(`options.`) ) {
                statePath = `${this._adapter.namespace}.${statePath}`;
            }
            const statePathSplit = statePath.split('.');
            const stOptionTable = statePathSplit[3]; // Like 'Zones'
            const stOptionName = statePathSplit.slice(4, statePathSplit.length-1).join('.'); // Like 'Hallway' or '.Zones.Flur EG.Lichter.Wandlicht 1'
            const stOptionField = statePathSplit[statePathSplit.length-1]; // Like 'active'
            let cName; // The final Option name from state 'smartcontrol.0.options.Zones.Hallway.name'
            try {
                const state = await this._adapter.getStateAsync(`options.${stOptionTable}.${stOptionName}.name`);
                if (state) {
                    cName = state.val;
                } else {
                    throw(`Unable to get state value of statePath '${statePath}'`);
                }
            } catch (error) { 
                this.dumpError(`Error getting state 'options.${stOptionTable}.${stOptionName}.name'`, error);
                return {};
            }
            
            // Find option table index
            let index = -1;
            if(stOptionTable == 'Schedules') {
                // Different handling for "tabSchedules" since we have '01_xxx', '02_xxx', etc. as name.
                const num = parseInt(stOptionName.substr(0,2));
                if(this.isNumber(num)) {
                    index = num-1;
                } else {
                    throw(`We were not able to convert leading 'xx_' of '${stOptionField}' to a number.`);
                }

            } else {
                for (let i = 0; i < this._adapter.config['table' + stOptionTable].length; i++) {
                    if (this._adapter.config['table' + stOptionTable][i].name == cName) {
                        // We have a hit.
                        index = i;
                        break;
                    }
                }
                if (index == -1) {
                    throw(`Unable to find option name '${cName}' in adapter settings, table '${'table' + stOptionTable}'.`);
                }
            }

            // Check if the actual key, like 'active', exists in adapter options object
            if (! (stOptionField in this._adapter.config['table'+stOptionTable][index]) ) {
                throw(`Key '${stOptionField}' not found in adapter settings, table '${'table' + stOptionTable}'.`);
            }

            return {
                name:       cName, 
                index:      index, 
                table:      'table'+stOptionTable, 
                tableName:  stOptionTable,
                field:      stOptionField, 
                row:        this._adapter.config['table'+stOptionTable][index]
            };

        } catch (error) {
            this.dumpError('Error', error);
        }

    }


    /**
     * Update option states. Required if admin options are being changed.
     */
    async updateOptionStatesFromConfig() {

        try {

            const states = this.generateOptionStates(false, false);

            for (const lpStatePath of states) {

                // {name:'Hallway', index:2, table:'tableZones', field:'active', row:{.....} }
                const optionObj = await this.asyncGetOptionForOptionStatePath(lpStatePath);

                // Set the state
                let val;
                if(typeof optionObj.row[optionObj.field] == 'object') {
                    val = JSON.stringify(optionObj.row[optionObj.field]);
                } else {
                    val = optionObj.row[optionObj.field];
                }
                await this._adapter.setStateAsync(lpStatePath, {val:val, ack:true});

            }

        } catch (error) {
            this.dumpError('[updateOptionStatesFromConfig()]', error);
            return;
        }
        
    }

    /**
     * Set timeout to switch devices of a zone off.
     * 
     * If a motion sensor was triggered and switch off time (duration) is provided.
     * 
     *  @param {object} cP         - Configuration parameters per getTriggerConfigParamAsync()
     *  @param {string} zoneName   - Zone Name
     *  @param {string} timerType  - 'motion' for motion timer, 'alwaysOff' for always off timer
     * 
     */
    async asyncSetZoneTimer(cP, zoneName, timerType) {

        try {

            // Go out if no duration is set
            if (timerType == 'motion' && (!cP.motionDuration || cP.motionDuration <= 0 )) return;

            // some constants we need later.
            const triggeredDevices = this.getOptionTableValue('tableZones', 'name', zoneName, 'triggers');
            const targetDevices = this.getOptionTableValue('tableZones', 'name', zoneName, 'targets');
            const offAfterSecs = parseInt(this.getOptionTableValue('tableZones', 'name', zoneName, 'offAfter'));

            // Get all target device state paths and values
            const targets = {
                statePaths: Array(), // Would use [] instead of Array(), but see: https://stackoverflow.com/a/56394664
                stateValues: Array(),
                deviceNames: Array(),
            };

            for(const lpTargetName of targetDevices) {
                // Get target device information
                const targetOffState = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetName, 'offState');
                const targetOffVal   = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetName, 'offValue');

                if(targetOffState == undefined || targetOffVal == undefined) {
                    throw(`Unable to retrieve target off state/value for target device '${lpTargetName}'`);
                }


                // Switching off state and state value is optional. So don't proceed if state is blank.
                /* // No longer optional (14 Jul 2020)
                if (this.isLikeEmpty(targetOffState.trim())) {
                    this._adapter.log.debug(`Trigger ${cP.triggerName}: No 'off state' is defined for ${lpTargetName}, so this device will not be turned off once timeout is reached.`);
                    continue;
                }
                */
                // Target Off State
                if (await this._adapter.getForeignObjectAsync(targetOffState)) {
                    // State is existing
                    targets.statePaths.push(targetOffState);
                    targets.stateValues.push(targetOffVal);
                    targets.deviceNames.push(lpTargetName);
                } else {
                    // State is not existing
                    this._adapter.log.error(`State '${targetOffState}' of device '${lpTargetName}' does not exist.`);
                }
            }

            // Go out if we actually have no devices to turn off
            if (this.isLikeEmpty(targets.statePaths)) {
                throw(`Trigger ${cP.triggerName}: --> No target states found in adapter settings, so no timer will be set to turn off.`);
            }

            /**
             * Timer
             */
            // Set new timeout to turn off all target states once timeout reached.
            switch (timerType) {
                case 'motion':

                    // Kill any triggered devices motion timers of the zone
                    for (const lpTriggerName of triggeredDevices) {
                        if (this._timers.motion[lpTriggerName]) {
                            const timeLeft = this.getTimeoutTimeLeft(this._timers.motion[lpTriggerName]);
                            if (timeLeft > -1) {
                                this._adapter.log.debug(`Zone '${zoneName}': Timer for ${lpTriggerName} still running ${(timeLeft/1000).toFixed(0)} seconds but a motion from ${lpTriggerName} detected, which is associated to same zone. Therefore, we stop timer and set new.`);
                                clearTimeout(this._timers.motion[lpTriggerName]);
                                this._timers.motion[lpTriggerName] = null;
                            }
                        }
                    }    
                    this._timers.motion[cP.triggerName] = setTimeout(async ()=> {
                        await this.asyncSetZoneTimer_doOnTimeout(cP, timerType, targets, cP.motionDuration);
                    }, cP.motionDuration * 1000);                    
                    break;
            
                case 'alwaysOff':
                    
                    if (!offAfterSecs || offAfterSecs < 1) {
                        break; // Go out if no seconds                        
                    } else {
                        // Kill timer
                        const timeLeft = this.getTimeoutTimeLeft(this._timers.zoneOff[zoneName]);
                        if (timeLeft > -1) {
                            clearTimeout(this._timers.zoneOff[zoneName]);
                            this._timers.zoneOff[zoneName] = null;
                        }
                        // Set offAfter timer
                        this._timers.zoneOff[zoneName] = setTimeout(async ()=> {
                            await this.asyncSetZoneTimer_doOnTimeout(cP, timerType, targets, offAfterSecs);
                        }, offAfterSecs * 1000);

                        if(timeLeft > -1) {
                            this.logExtendedInfo(`'alwaysOff' ${offAfterSecs}s timer still running ${(timeLeft/1000).toFixed(0)}s. We stop it and set new to ${offAfterSecs}s.`);
                        } else {
                            this.logExtendedInfo(`'alwaysOff' ${offAfterSecs}s timer for zone '${zoneName}' initially started.`);
                        }
                        break;
                    }

                default:
                    throw (`timer type ${timerType} not defined in function.`);
            }
            
        } catch (error) {
            this.dumpError(`[asyncSetZoneTimer]`, error);
            return;
        }

    }


    /**
     *  @param {object} cP        - Configuration parameters per getTriggerConfigParamAsync()
     *  @param {string} timerType - For logging only. Suggestion: 'motion' for motion timer, 'alwaysOff' for always off timer
     *  @param {object} targets   - {statePaths:['x', 'y'], stateValues: ['m', 'n'], deviceNames: ['a','b']};
     *  @param {number} secTimer  - Number of seconds of the timer, that was set and now expired. For logging only.
     */
    async asyncSetZoneTimer_doOnTimeout(cP, timerType, targets, secTimer) {

        try {

            const lTargetDevicesSwitchedOff = []; // For Log
            const lManualOnDevices = []; // For log
            for (let i = 0; i < targets.statePaths.length; i++) {
    
                // All are having same length and association per each element
                const lpStatePath = targets.statePaths[i];
                const lpStateValToSet = targets.stateValues[i];
                const lpTargetName = targets.deviceNames[i];
                
                // Remove from this._motionTriggeredDevices
                if (cP.motionNotIfManual) this._motionTriggeredDevices = this.arrayRemoveElementByValue(this._motionTriggeredDevices, lpTargetName);

                // Check if current value != new value
                const targetOffStateValCurrent = await this.asyncGetForeignStateValue(lpStatePath);
                if (targetOffStateValCurrent == null) {
                    this._adapter.log.error(`Timer '${timerType}', trigger '${cP.triggerName}', target ${lpTargetName}: - Timeout of '${secTimer}' seconds reached, but could not get current state value of '${lpStatePath}', so unable to turn device off.`);
                    continue;
                }
                if (lpStateValToSet == targetOffStateValCurrent) {
                    this.logExtendedInfo(`Timer '${timerType}', trigger '${cP.triggerName}', target ${lpTargetName}: - Timeout of '${secTimer}' seconds reached, target is already turned off, so not turning off again.`);
                    continue;
                }
    
                // All passed.
                await this._adapter.setForeignStateAsync(lpStatePath, {val: lpStateValToSet, ack: false });
                lTargetDevicesSwitchedOff.push(lpTargetName);
    
            }
    
            // Log
            if (lManualOnDevices.length > 0) {
                this.logExtendedInfo(`Timer '${timerType}', trigger '${cP.triggerName}', target(s) '${lManualOnDevices.toString()}': - Timeout of '${secTimer}' seconds reached, target(s) manually turned on while timer was running, so do not turn off.`);
            }
            if (lTargetDevicesSwitchedOff.length > 0) {
                this.logExtendedInfo(`Timer '${timerType}', trigger '${cP.triggerName}', target(s) '${lTargetDevicesSwitchedOff.toString()}': - Turned off per ${secTimer}s timer.`);
            }                    


        } catch (error) {
            this.dumpError('[asyncSetZoneTimer_doOnTimeout]', error);
            return;
        }

    }



    /**
     * For given triggerStatePath and associated schedule rows, get all needed values.
     * Also ensures, that all schedule conditions are met.
     * 
     * @param {string} triggerName    - Name of trigger.
     * @return {Promise<object|null>}          - The return object. We return null in case of errors
     */
    async getTriggerConfigParamAsync(triggerName) {

        try {

            // The return object
            const cP = {
                // {string}  triggerName            Name of Trigger
                // {string}  triggerTableId         table name: 'tableTriggerDevices', 'tableTriggerMotion', or 'tableTriggerTimes'
                // {string}  triggerStatePath       State Path of Trigger, if a state
                // {string}  triggerStateVal        Trigger state value from Options Table
                // {string}  triggerStateValSet     Trigger state value which was set in triggerStatePath
                // {string}  triggerIsToggle        Trigger is a toggle, so isToggle is true in tableTriggerDevices
                
                // {boolean} targetOff              If true, targets will be switched off, and not on.

                // {boolean} triggerIsMotion        Is trigger a motion sensor?

                // {string}  triggerTime            If time trigger: time like '42 * * * *' or 'sunset+30'
                // {array}   triggerTmAdditionCond  Array of additional condition names            
                // {string}  triggerTmNever         Array of condition names which must not be met

                // {number}  motionDuration         Duration (timer) is seconds for motion sensor
                // {string}  motionBriStatePath     State Path of brightness state
                // {number}  motionBriThreshold     Threshold of brightness
                // {boolean} motionNotIfManual      if true: motion trigger should not set timeout if target device was turned on previously without a motion trigger.

                // {array}   zoneNames              All zone names that were triggered and meet all conditions
                // {object}  zoneTargetNames        All target device names per zone, like {Hallway:['Hallway Light'], Bath:['Light', 'Radio Bath']}
                
            };

            cP.triggerName = triggerName;
            cP.triggerStatePath = undefined; // set below accordingly if state
            cP.triggerStateVal = undefined;  // set below accordingly if state
            cP.triggerIsMotion = false; // set to true below, if motion
            cP.triggerIsToggle = false; // set to true below, if tableTriggerDevices.isToggle=true
            cP.targetOff = false; // set to true below, if activated in other devices or time trigger table

            /**
             * Get all triggered table rows and set values accordingly.
             */
            const result = { row:{}, tableId:'' };
            for (const lpTable of ['tableTriggerDevices', 'tableTriggerMotion', 'tableTriggerTimes']) {
                for (const lpRow of this._adapter.config[lpTable]) {
                    if (lpRow.active && lpRow.name == triggerName) {
                        result.row = lpRow;
                        result.tableId = lpTable;
                        break;
                    }
                }
                if (result.index > -1) break;
            }

            switch (result.tableId) {

                case 'tableTriggerDevices':
                    cP.triggerTableId = result.tableId;
                    cP.triggerStatePath = result.row.stateId;
                    cP.triggerStateVal = result.row.stateVal;
                    cP.triggerIsToggle = result.row.isToggle;
                    cP.targetOff = result.row.targetOff;                    
                    break;

                case 'tableTriggerMotion':
                    cP.triggerTableId = result.tableId;    
                    cP.triggerIsMotion = true;    
                    cP.triggerStatePath = result.row.stateId;
                    cP.triggerStateVal = result.row.stateVal;
                    cP.motionNotIfManual = result.row.notIfManual;
                    if (result.row.duration && parseInt(result.row.duration) > 0 ) {
                        cP.motionDuration = parseInt(result.row.duration);
                    } else  {
                        cP.motionDuration = 0;
                    }
                    if (result.row.briThreshold && parseInt(result.row.briThreshold) > 0 && result.row.briStateId && result.row.briStateId.length > 5 ) {
                        cP.motionBriStatePath = result.row.briStateId;
                        cP.motionBriThreshold = result.row.briThreshold;
                    } else {
                        cP.motionBriStatePath = '';
                        cP.motionBriThreshold = 0;
                    }                  
                    break;

                case 'tableTriggerTimes':
                    cP.triggerTableId = result.tableId;
                    cP.targetOff = result.row.targetOff; 
                    cP.triggerTime = result.row.time;
                    cP.triggerTmAdditionCond = result.row.additionalConditions;
                    cP.triggerTmNever = result.row.never;                  
                    break;

                default:
                    throw (`No active trigger '${triggerName}' found in any trigger table.`);
            }

            /**
             * Get zone names, and zone-specific target names and execution
             * Loop next iteration, if not successful.
             */
            cP.zoneNames = [];        // Zone names that were triggered and which match with a schedule row name
            cP.zoneTargetNames = {};  // Target devices per zone, like {Hallway:['Hallway Light'], Bath:['Bath Light', 'Bath Radio']}

            for (const lpRowZones of this._adapter.config.tableZones) {

                // Loop next if item in table "Zones" is not containing any trigger name in column triggers
                if (! (lpRowZones.active && lpRowZones.triggers && Array.isArray(lpRowZones.triggers) && lpRowZones.triggers.includes(triggerName)) ) continue; 

                // Add target devices
                cP.zoneTargetNames[lpRowZones.name] = lpRowZones.targets;

                // Execution options
                //const executeAlways    =    this._adapter.config.execution[lpRowZones.name].executeAlways;
                const executeAlways = (lpRowZones.executeAlways) ? true : false;
                let tablesZoneExecution = [];
                if (executeAlways) {
                    // we create a dummy to always execute
                    // TODO: remove this dummy and implement it more beautiful
                    tablesZoneExecution = [{
                        active: true,
                        start: '0:00',
                        end: '24:00',
                        mon: true,
                        tue: true,
                        wed: true,
                        thu: true,
                        fri: true,
                        sat: true,
                        sun: true,
                        additionalConditions: [],
                        never:[]                      
                    }];
                } else {
                    tablesZoneExecution = (lpRowZones.executionJson) ? JSON.parse(lpRowZones.executionJson) : [];
                }

                for (const lpRow of tablesZoneExecution) {

                    if (lpRow.active) {

                        this._adapter.log.debug(`=== check schedule conditions ===`);

                        // A few variables / constants
                        const tsCurrent = Date.now();   // the current timestamp
                        let doContinue = true;          // flag if we can continue.
                
                        // First – check if current time is within the schedule times        
                        if (doContinue) doContinue = this.scheduleIsTimeStampWithinPeriod(tsCurrent, lpRow.start, lpRow.end, lpRowZones.name);
                
                        // Next, check if current day is within the mon-sun options as set in schedules tables.
                        if (doContinue) doContinue = this.scheduleIsWeekdayMatching(lpRow, tsCurrent, lpRowZones.name);
                
                        // Next, check additional conditions (like Public Holiday, etc. -- as set in conditions table)
                        if (doContinue) doContinue = await this.asyncAreScheduleConditionsMet(cP, lpRow.additionalConditions, true);
                
                        // Next, check conditions "Never if"
                        if (doContinue) doContinue = await this.asyncAreScheduleConditionsMet(cP, lpRow.never, false, true);
                
                        // All checks done.
                        if (doContinue) {
                            this._adapter.log.debug(`Zone '${lpRowZones.name}' - execution table row *is* meeting conditions`);
                            /* isHit == true; */
                            cP.zoneNames.push(lpRowZones.name); // set matching zone name
                        } else {
                            this._adapter.log.debug(`Zone '${lpRowZones.name}' - execution table row is *not* meeting conditions`);
                        }  

                    }

                }

            }

            if (this.isLikeEmpty(cP.zoneNames) || this.isLikeEmpty(cP.zoneTargetNames)) {
                this._adapter.log.debug(`Trigger '${triggerName}': activation failed - no active row for this trigger found in Zones, or Zone is not active in Execution table.`);
                return null;
            } else {
                this._adapter.log.debug(`${cP.zoneNames.length} rows in Zones table for trigger '${triggerName}' found and assigned execution table rows fetched successfully.`);
            }

            // Return result
            return cP;

        } catch (error) {
            this.dumpError('[getTriggerConfigParamAsync()]', error);
            return null;
        }

    }



    /**
     * Called once a trigger was activated (i.e. a motion or other trigger state changed)
     * @param {string}                        statePath      State Path of the trigger
     * @param {ioBroker.State|null|undefined} stateObject    State object
     */
    async switchTargetsPrepareAsync(statePath, stateObject) {

        try {

            if (!stateObject || !statePath || stateObject.val == null) return;

            // First of all, retrieve all trigger names which contain the statePath (per asyncVerifyConfig(), names are unique)
            const allTriggerRows = this._adapter.config.tableTriggerMotion.concat(this._adapter.config.tableTriggerDevices);
            const triggerNames = [];
            for (const lpTriggerRow of allTriggerRows) {
                if (lpTriggerRow.active && lpTriggerRow.stateId == statePath) {
                    triggerNames.push(lpTriggerRow.name);
                }
            }
            if (triggerNames.length == 0) {
                throw(`No trigger found for '${statePath}'`);
            }

            // Loop through all trigger names
            for (const lpTriggerName of triggerNames) {
  
                const cP = await this.getTriggerConfigParamAsync(lpTriggerName);
                if (cP == null) {
                    this._adapter.log.debug(`Trigger '${lpTriggerName}': Trigger not meeting schedule or no valid config parameters found for ${statePath}`);
                    continue;
                }

                this._adapter.log.debug(`[TriggerConfigParam (cP)] : ${JSON.stringify(cP)}`);

                /**
                 * Verify if state value that was set matches with the config
                 */
                // Check for >=, <=, >, <  and number, so like '>= 3', '<7'
                let comparatorSuccess = false;
                if (cP.triggerTableId == 'tableTriggerDevices' && (typeof cP.triggerStateVal == 'string') ) {
                    const matchComparator = cP.triggerStateVal.match(/^(>=|<=|>|<)\s?(\d{1,})$/);
                    if (matchComparator != null) {
                        // String like ">= 30" found.
                        const operand = matchComparator[1];
                        const num = parseFloat(matchComparator[2]);
                        if (operand == '>=' && Number(stateObject.val) >= num ) {
                            comparatorSuccess = true;
                        } else if (operand == '<=' && Number(stateObject.val) <= num ) {
                            comparatorSuccess = true;
                        } else if (operand == '>' && Number(stateObject.val) > num ) {
                            comparatorSuccess = true;
                        } else if (operand == '<' && Number(stateObject.val) < num ) {
                            comparatorSuccess = true;
                        } else {
                            this._adapter.log.debug(`Trigger '${lpTriggerName}' activated, but not meeting 'comparator' condition (trigger state val: '${cP.triggerStateVal}'), therefore, we disregard the activation.`);
                            continue; // We go out since we have a comparator, but not matching
                        }
                    }

                }
                // Compare state value with config            
                if (!comparatorSuccess && cP.triggerStateVal != undefined && cP.triggerStateVal != stateObject.val ) {
                    this._adapter.log.debug(`Trigger '${lpTriggerName}' activated, but not meeting conditions (triggerStateVal '${cP.triggerStateVal}' != stateObject.val '${stateObject.val}' ), therefore, we disregard the activation.`);
                    continue; // Go out since no match
                }
                
                // A motion sensor was the triggered and a brightness is set and if state bri is smaller than defined threshold.
                if (cP.triggerIsMotion
                    && cP.motionBriStatePath && !this.isLikeEmpty(cP.motionBriStatePath)
                    && cP.motionBriThreshold && !this.isLikeEmpty(cP.motionBriThreshold)
                    && this.isNumber(cP.motionBriThreshold) && parseInt(cP.motionBriThreshold) > 0 ) {
    
                    const disregardBriIfTimerIsRunning = this._adapter.config.motionNoBriIfTimer;
                    const isTimerRunning = (this.getTimeoutTimeLeft(this._timers.motion[cP.triggerName]) <= 0) ? false : true;
                    if (disregardBriIfTimerIsRunning && isTimerRunning) {
                        // Disregard brightness if lamp is on
                        // Ist eine Lampe an, wird ein größerer Lux-Wert vom Sensor 
                        // gemessen, daher macht die Prüfung auf Lux bei bereits eingeschalteter Lampe auf erneute Bewegung und Lux-Abfrage 
                        // keinen Sinn mehr. Daher wird durch dieses Script bei Bewegung während das Gerät (die Lampe) an ist -- die Lux-Erkennung 
                        // hier deaktiviert.
                        this._adapter.log.debug(`Trigger [${cP.triggerName}] Timer to turn devices off for this motion sensor is still running. New motion detected in the meanwhile, therefore, we disregard any current brightness states since a turned on light would break the brightness measurement.`);
                        // continue (disregard bri)
                    } else {
                        // Do the brightness stuff
                        const briStateObjCurr = await this.asyncGetForeignState(cP.motionBriStatePath); // {val: false, ack: true, ts: 1591117034451, …}
                        const briStateValCurrent = briStateObjCurr.val;
                        if ( this.isLikeEmpty(briStateValCurrent) || !this.isNumber(briStateValCurrent) || parseInt(briStateValCurrent) < 0) {
                            // Bri not valid
                            this._adapter.log.debug(`Trigger [${cP.triggerName}] Brightness of ${briStateValCurrent} of ${cP.motionBriStatePath} is not valid. State Value: [${briStateValCurrent}]. Therefore, bri will be disregarded and we continue.`);
                            // We continue anyway
                        } else if (parseInt(briStateValCurrent) < parseInt(cP.motionBriThreshold)) {
                            // continue
                            this._adapter.log.debug(`Trigger [${cP.triggerName}] Brightness of ${briStateValCurrent} is < threshold of ${cP.motionBriThreshold}, so we continue.`);
                        } else {
                            // Brightness condition is not met!
                            this.logExtendedInfo(`Trigger [${cP.triggerName}] activated but current brightness of ${briStateValCurrent} is >= 10 (= setting threshold of ${cP.motionBriThreshold}) -> Motion disregarded.`);
                            continue;
                        }
                    }
    
                } else if (cP.triggerIsMotion) {
                    this._adapter.log.debug(`No brightness defined for motion sensor '${cP.triggerName}', so continue and do not use bri as an additional criterion.`);
                }
    
                // Do not switch more often than xxx seconds
                let threshold = parseInt(this._adapter.config.limitTriggerInterval); // in seconds
                if(!threshold || !this.isNumber(threshold) || threshold < 1) threshold = 1;
    
                const formerTs = this._onStateChangeTriggers[cP.triggerName];
                const currTs = Date.now();
                this._onStateChangeTriggers[cP.triggerName] = currTs;
                if (formerTs && ( (formerTs + (threshold*1000)) > currTs)) {
                    this._adapter.log.info(`Trigger '${cP.triggerName}' was already activated ${Math.round(((currTs-formerTs) / 1000) * 100) / 100} seconds ago and is ignored. Must be at least ${threshold} seconds.`);
                    continue;
                }
    
                // Finally, switch:
                this.switchTargetsExecuteAsync(cP);

            }

        } catch (error) {
            this.dumpError('[switchTargetsPrepareAsync()]', error);
            return false;
        }

    }

    /**
     * Switch all target devices.
     * * Note: No need to remove duplicates, this is being taken care of with asyncVerifyConfig() function.
     * 
     * @param {object} cP         - Configuration parameters per getTriggerConfigParamAsync()
     */
    async switchTargetsExecuteAsync(cP) {

        try {

            for (const lpZoneName of cP.zoneNames) {
                
                // Loop thru the target device names and switch accordingly.
                const lSwitchedDevices = [];
                const lAlreadyOnDevices = [];
                let onOff = 'on'; // for toggle

                // If "target on" value is overwritten per tableZones.targetsOverwrite
                const targetsOverwrite = this.getOptionTableValue('tableZones', 'name', lpZoneName, 'targetsOverwrite'); // like: {'Hallway.Light':'new val' 'Hallway.Radio':'Radio XYZ'}

                for (const lpTargetDeviceName of cP.zoneTargetNames[lpZoneName]) {

                    // If target device should be switched off, and not on, per config in trigger table
                    // Will be overwritten later if toggle is activated
                    if (cP.targetOff) onOff = 'off';

                    // Toggle
                    if (cP.triggerIsToggle) {
                        // If we have Toggle, we are using the ON state/value to check.
                        const onState = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetDeviceName, 'onState');
                        const onVal   = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetDeviceName, 'onValue');                    
                        const oldStateVal = await this.asyncGetForeignStateValue(onState);                                        
                        if (oldStateVal == null) throw(`Could not get current state value for '${onState}' of device '${lpTargetDeviceName}'`);
                        if (onVal == oldStateVal) {
                            onOff = 'off';
                        } else {
                            onOff = 'on';
                        }
                    }

                    // Get option values.
                    const lpTargetState = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetDeviceName, onOff+'State');
                    let lpTargetVal   = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetDeviceName, onOff+'Value');
                    const noTargetCheckStr = (onOff=='on') ? 'noTargetOnCheck' : 'noTargetOffCheck';
                    const doNotCheckTarget = this.getOptionTableValue('tableTargetDevices', 'name', lpTargetDeviceName, noTargetCheckStr);

                    if(lpTargetState == undefined || lpTargetVal == undefined) {
                        throw(`Unable to retrieve target '${onOff}' state/value for target device '${lpTargetDeviceName}'`);
                    }

                    // Overwrite target values if according value is set in Zones table
                    if (onOff=='on' && targetsOverwrite && !this.isLikeEmpty(targetsOverwrite[lpTargetDeviceName])) {
                        lpTargetVal = targetsOverwrite[lpTargetDeviceName];
                    }

                    // Let's get old state value
                    let oldStateVal;
                    if (!doNotCheckTarget) {
                        oldStateVal = await this.asyncGetForeignStateValue(lpTargetState);
                        if (oldStateVal == null) {
                            throw(`Could not get current state value for '${lpTargetState}' of device '${lpTargetDeviceName}'`);
                        }
                    }

                    // Set target state.
                    // Note: Verification of the state value and conversion as needed was performed already by asyncVerifyConfig()
                    // Check if device is already on.
                    if (doNotCheckTarget || (!doNotCheckTarget && (oldStateVal != lpTargetVal))) {
                        // Set state
                        await this._adapter.setForeignStateAsync(lpTargetState, {val: lpTargetVal, ack: false });
                        lSwitchedDevices.push(lpTargetDeviceName);   
                        if (cP.motionNotIfManual) {
                            if (cP.triggerIsMotion) {
                                // triggered by motion, so add to this._motionTriggeredDevices
                                if ( this._motionTriggeredDevices.indexOf(lpTargetDeviceName) == -1 ) {
                                    this._motionTriggeredDevices.push(lpTargetDeviceName);
                                }
                            } else {
                                //Triggered by NON-motion, so remove from this._motionTriggeredDevices
                                this._motionTriggeredDevices = this.arrayRemoveElementByValue(this._motionTriggeredDevices, lpTargetDeviceName);
                            }
                        }

                    } else {
                        // do not set target state
                        lAlreadyOnDevices.push(lpTargetDeviceName);
                    }
                
                }

                // Set Motion Sensor timer
                let logMotion = '';
                const cP_motion = { ...cP }; // clone (copy) object. Needed since we alter cP.zoneTargetNames[lpZoneName] for motion timer
                if (cP.triggerIsMotion) {

                    // If target was already turned on previously by a non-motion trigger: do not schedule timer.
                    const lRemovedDevices = [];
                    if (cP.motionNotIfManual) {
                        for (const lpTargetDevice of lAlreadyOnDevices) {
                            if ( this._motionTriggeredDevices.indexOf(lpTargetDevice) > -1 ) {
                                // Device was already on and motion triggered before.
                            } else {
                                // Device was already on, but NOT motion triggered before.
                                // Therefore, we will not set a timer
                                cP_motion.zoneTargetNames[lpZoneName] = this.arrayRemoveElementByValue(cP_motion.zoneTargetNames[lpZoneName], lpTargetDevice);
                                lRemovedDevices.push(lpTargetDevice);                        
                            }
                        }
                    }

                    // Set Motion Timer
                    if (cP_motion.zoneTargetNames[lpZoneName].length < 1) {
                        logMotion = `No motion timer set to turn off since device(s) turned on manually before.`;
                    } else {
                        if (lRemovedDevices.length == 0) {
                            logMotion = `Setting timer of ${cP.motionDuration}s to turn off.`;
                        } else {
                            logMotion = `Setting timer of ${cP.motionDuration}s to turn off, limited to '${cP_motion.zoneTargetNames[lpZoneName].toString()}'. ${lRemovedDevices.toString()}  turned on manually before.`;
                        }
                        // Now set timer 
                        this.asyncSetZoneTimer(cP_motion, lpZoneName, 'motion');
                    }
                } 

                /**
                 * Set "always off after" timer if set in Table Zones.
                 */
                if (cP.triggerIsToggle && onOff == 'off') {
                    // Toggle is activated and zone is turned off, so kill/delete the timer.
                    const alwaysOffTimeLeft = this.getTimeoutTimeLeft(this._timers.zoneOff[lpZoneName]);
                    if (alwaysOffTimeLeft > -1 ) {
                        this._adapter.log.debug(`Zone '${lpZoneName}': Turn off 'always off' timer, since still running ${(alwaysOffTimeLeft/1000).toFixed(0)} seconds and toggle trigger triggered to turn off.`);
                        clearTimeout(this._timers.zoneOff[lpZoneName]);
                        this._timers.zoneOff[lpZoneName] = null;                        
                    }

                } else {
                    if (cP.triggerIsMotion && (cP_motion.zoneTargetNames[lpZoneName].length < 1) ) {
                        this.logExtendedInfo(`Trigger '${cP.triggerName}', Zone '${cP.zoneNames.toString()}': no 'always off after' timer set since device(s) turned on manually before.`);
                    } else {
                        this.asyncSetZoneTimer(cP, lpZoneName, 'alwaysOff');
                    }
                }

                // Finalize
                if (lSwitchedDevices.length > 0) {
                    if (lAlreadyOnDevices.length < 1) {
                        this.logExtendedInfo(`Trigger '${cP.triggerName}' activated Zone '${lpZoneName}'. Turned ${onOff}: ${lSwitchedDevices.toString()}.`);
                        if (logMotion) this.logExtendedInfo(`Trigger '${cP.triggerName}', Zone '${lpZoneName}': ${logMotion}`);
                        
                    } else {
                        this.logExtendedInfo(`Trigger '${cP.triggerName}' activated Zone '${lpZoneName}'. Turned ${onOff}: ${lSwitchedDevices.toString()}. Not turned ${onOff} (as already ${onOff}): '${lAlreadyOnDevices.toString()}'`);
                        if (logMotion) this.logExtendedInfo(`Trigger '${cP.triggerName}', Zone '${lpZoneName}': ${logMotion}`);
                    }
                } else {
                    this.logExtendedInfo(`Trigger '${cP.triggerName}' activated Zone '${lpZoneName}'. However, devices '${lAlreadyOnDevices.toString()}' not turned ${onOff} as these are already ${onOff}.`);
                    if (logMotion) this.logExtendedInfo(`Trigger '${cP.triggerName}', Zone '${lpZoneName}': ${logMotion}`);
                }



            }

            // Done.
            return true;

        } catch (error) {
            this.dumpError('[switchTargetsExecuteAsync()]', error);
            return false;
        }

    }


    /**
     * Check if additional conditions are met (like Public Holiday, etc. -- as set in conditions table).
     * 
     * @param {object} cP   Configuration parameters per getTriggerConfigParamAsync()
     * @param {array}  conditionNames   Array of condition names
     * @param {boolean} allMustMeet    if true: all conditions must be met, if false: one condition is enough
     * @param {boolean} [inverse]      if true: inverse results (true -> false, and vice versa)
     * @return {Promise<boolean>}  True if condition(s) met, false if not. Also return true, if empty conditionNames array
     */
    async asyncAreScheduleConditionsMet(cP, conditionNames, allMustMeet, inverse=false) {

        try {

            // If empty array, we return true, as we need to continue if no conditions are set in the options table.
            if (this.isLikeEmpty(conditionNames) ) {
                this._adapter.log.debug(`${cP.triggerName}: No extra condition(s) in schedule, so we return "true" to continue.`);
                return true;
            }

            let hitCount = 0;

            this._adapter.log.debug(`${cP.triggerName}: Check if additional conditions are met (like Public Holiday) - Processing conditions '${JSON.stringify(conditionNames)}'`);

            for (const lpConditionName of conditionNames) {

                if (hitCount > 0 && allMustMeet == false) break;
                
                const lpConditionStatePath = this.getOptionTableValue('tableConditions', 'name', lpConditionName, 'conditionState');
                const lpConditionStateValue = this.getOptionTableValue('tableConditions', 'name', lpConditionName, 'conditionValue');

                if(!lpConditionStatePath) {
                    this._adapter.log.error(`asyncAreScheduleConditionsMet(): condition details for '${lpConditionName}' could not be found.`);
                    if (allMustMeet) {break;} else {continue;}
                }

                const lpStateVal = await this.asyncGetForeignStateValue(lpConditionStatePath);
                if (lpStateVal == null) {
                    if (allMustMeet) {break;} else {continue;}
                }


                // Finally, compare the config table state value with the actual state value
                if (lpConditionStateValue == lpStateVal) {
                    this._adapter.log.debug(`${cP.triggerName}: Condition '${lpConditionName}' *met* (options val = '${lpConditionStateValue}', actual state val = '${lpStateVal}')`);
                    hitCount++;
                    if (allMustMeet) {continue;} else {break;}                
                } else {
                    // No hit, so go out!
                    this._adapter.log.debug(`${cP.triggerName}: Condition '${lpConditionName}' *not* met (options val = '${lpConditionStateValue}', actual state val = '${lpStateVal}')`);
                    if (allMustMeet) {break;} else {continue;}                
                }

            }

            let result = false;
            if ( (allMustMeet && ( hitCount == conditionNames.length )) || (!allMustMeet && ( hitCount > 0 )) ) 
                result = true;

            if (inverse) result = !result;

            if(result) {
                this._adapter.log.debug(`${cP.triggerName}: additional conditions check - final result = matching`);
                return true;
            } else {
                this._adapter.log.debug(`${cP.triggerName}: additional conditions check - final result = *not* matching`);
                return false;
            }

        } catch (error) {
            this.dumpError('[asyncAreScheduleConditionsMet()]', error);
            return false;
        }

    }

    /**
     *    ██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████  █████  ████████ ██  ██████  ███    ██ 
     *    ██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██   ██    ██    ██ ██    ██ ████   ██ 
     *    ██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   ███████    ██    ██ ██    ██ ██ ██  ██ 
     *    ██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██   ██    ██    ██ ██    ██ ██  ██ ██ 
     *    ██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ██   ██    ██    ██  ██████  ██   ████ 
     *
     *    Adapter initialization
     */

    /**
     * Verify adapter configuration table
     * @param {array} functionConfigArray    Array of what to check
     * @return {Promise<boolean>}   passed   Returns true if no issues, and false if error(s).
     */
    async asyncVerifyConfig(functionConfigArray) {

        try {
            
            let errors = 0;
            let validTriggerCounter = 0;

            // FIRST: Check the config array
            for (const functionConfigObj of functionConfigArray) {

                /**
                 * Prepare the table(s). Typically we only have one table to check, however, 'tableZoneExecution' requires
                 * multiple tables (per each row of Table Zones)
                 */
                const tablesToCheck = []; // Array holding all tables to check. Will be just one element, except to 'tableZoneExecution'
                if (!(functionConfigObj.tableId == 'tableZoneExecution')) {
                    // settings of adapter config table, like of 'tableTargetDevices'
                    tablesToCheck.push({table:this._adapter.config[functionConfigObj.tableId]}); 
                } else {
                    
                    let counter = 0;
                    for (const lpZoneRow of this._adapter.config.tableZones) {
                        if (lpZoneRow.active) {
                            const tbl = (this.isLikeEmpty(lpZoneRow.executionJson)) ? [] : JSON.parse(lpZoneRow.executionJson);
                            tablesToCheck.push({zoneTableRowNo:counter, executeAlways:lpZoneRow.executeAlways, table:tbl}); 
                        }
                        counter++;
                    }
                }

                /**
                 * Handle each table
                 */
                for (const lpTable of tablesToCheck) {
                    if (lpTable.executeAlways) continue; // Skip if executeAlways is activated

                    // For Zone Table Execution only: set "better" table name for log
                    if(lpTable.zoneTableRowNo != undefined) {
                        functionConfigObj.tableName = `Zone: ${this._adapter.config.tableZones[lpTable.zoneTableRowNo].name}, Execution`;
                    }

                    // Active status of each row; We simply push true or false in loop, same index as table rows (lpTable.table)
                    const rowActiveStatus = [];
                
                    if (this.isLikeEmpty(lpTable.table)) {
                        if(functionConfigObj.tableMustHaveActiveRows) {
                            this._adapter.log.warn('[Config Table \'' + functionConfigObj.tableName + '\'] No rows defined.');
                            errors++;
                        }
                        continue;
                    } 
                
                    for (let i = 0; i < lpTable.table.length; i++) {
                        // -- deactivated for 0.1.1-beta.2 - we should also validate de-activated table items, as these 
                        //    can be switched on thru smartcontrol.x.options.xxx.xxx.active
                        // if (!lpTable.table[i].active) continue;

                        // Returns all object keys starting with 'check_' as array, like ['check_1','check_2']. Or empty array, if not found.
                        const lpCriteriaToCheck = (Object.keys(functionConfigObj).filter(str => str.includes('check_'))); 
                        
                        for (const lpCheckKey of lpCriteriaToCheck) {

                            // lpCheckKey = check_1, or check_2, etc. 
                            // lpFcnConfigCheckObj = like {id: 'triggers', type:'name', deactivateIfError:true, removeForbidden:true }
                            const lpFcnConfigCheckObj = functionConfigObj[lpCheckKey]; 

                            // Special for tableTriggerDevices - userState
                            const isUserState = ((functionConfigObj.tableId == 'tableTriggerDevices') && lpTable.table[i].userState) ? true : false;

                            // Config table: the string from config table row, like '0.userdata.0.abc.def', 'true', 'false', 'Play Music', etc.
                            let lpConfigString = lpTable.table[i][lpFcnConfigCheckObj.id];

                            // +++++++ VALIDATION TYPE: a state path like "0_userdata.0.teststate" +++++++
                            if (lpFcnConfigCheckObj.type == 'statePath') {

                                lpConfigString = lpConfigString.trim();

                                if ( this.isLikeEmpty(lpConfigString) && lpFcnConfigCheckObj.optional) {
                                    // Skip if empty state and field is optional
                                    continue;
                                }

                                // Special treatment for tableTriggerDevices, User States
                                if (isUserState) {
                                    if(!lpConfigString.startsWith(this._adapter.namespace + '.userstates.')) {
                                        lpConfigString = this._adapter.namespace + '.userstates.' + lpConfigString;
                                    }
                                }

                                if (!this.isStateIdValid(lpConfigString)) {
                                    // State path is not valid
                                    this._adapter.log.warn(`[Config Table '${functionConfigObj.tableName}'] ${lpFcnConfigCheckObj.id} - State '${lpConfigString}' is not valid.`);
                                    if (lpFcnConfigCheckObj.deactivateIfError) {
                                        lpTable.table[i].active = false;
                                        rowActiveStatus[i] = false;
                                    }
                                    errors++; 
                                    continue;
                                }

                                if (!isUserState) {
                                    const lpStateObject = await this._adapter.getForeignObjectAsync(lpConfigString);
                                    if (!lpStateObject) {
                                        // State does not exist
                                        this._adapter.log.warn(`[Config Table '${functionConfigObj.tableName}'] State '${lpConfigString}' does not exist.`);
                                        if (lpFcnConfigCheckObj.deactivateIfError) {
                                            lpTable.table[i].active = false;
                                            rowActiveStatus[i] = false;
                                        }
                                        errors++; 
                                        continue;
                                    }
                                }

                                // We altered lpConfigString, so set to adapter config.
                                lpTable.table[i][lpFcnConfigCheckObj.id] = lpConfigString;
                                

                            // +++++++ VALIDATION TYPE: stateValue -- a value to be set to a certain state +++++++
                            } else if (lpFcnConfigCheckObj.type == 'stateValue') {

                                const lpStatePath       = lpTable.table[i][lpFcnConfigCheckObj.stateValueStatePath];
                                const lpStateVal        = lpConfigString;
                                const lpIsOptional      = lpFcnConfigCheckObj.optional;
                                const lpConfigTableName = functionConfigObj.tableName;
                                
                                const stateValueResult = await this.asyncVerifyConfig_verifyStateValue(lpStatePath, lpStateVal, lpIsOptional, lpConfigTableName);

                                if (stateValueResult.validationFailed) {
                                    if (lpFcnConfigCheckObj.deactivateIfError) {
                                        lpTable.table[i].active = false;
                                        rowActiveStatus[i] = false;
                                    }
                                    errors++; 
                                    continue;
                                } else {
                                    if ('newStateVal' in stateValueResult) {
                                        this._adapter.config[functionConfigObj.tableId][i][lpFcnConfigCheckObj.id] = stateValueResult.newStateVal;
                                    }
                                }
                    
                            // +++++++ VALIDATION TYPE: a number +++++++
                            } else if (lpFcnConfigCheckObj.type == 'number') {

                                const lpNumberToCheck = lpTable.table[i][lpFcnConfigCheckObj.id];

                                if (this.isLikeEmpty(lpNumberToCheck) && lpFcnConfigCheckObj.optional ) {
                                    // Skip if empty and if is optional. 
                                    continue;
                                }

                                if(! this.isNumber(lpNumberToCheck) ) {
                                    this._adapter.log.warn(`[Config Table ${functionConfigObj.tableName}] Field ${lpFcnConfigCheckObj.id} is expecting a number, but you set '${lpConfigString}'.`);
                                    if (lpFcnConfigCheckObj.deactivateIfError) lpTable.table[i].active = false;
                                    errors++; 
                                    continue;
                                // check for lower limit, if 'numberLowerLimit' set in functionConfigObj object
                                } else if (! this.isLikeEmpty(lpFcnConfigCheckObj.numberLowerLimit)) {
                                    if(parseInt(lpNumberToCheck) < lpFcnConfigCheckObj.numberLowerLimit) {
                                        this._adapter.log.warn('[Config Table \'' + functionConfigObj.tableName + '\'] Number in field "' + lpFcnConfigCheckObj.id + '" is smaller than ' + lpFcnConfigCheckObj.numberLowerLimit + ', which does not make sense.');
                                        if (lpFcnConfigCheckObj.deactivateIfError) {
                                            lpTable.table[i].active = false;
                                        }
                                        errors++; 
                                        continue;
                                    }
                                // check for upper limit, if 'numberUpperLimit' set in functionConfigObj object
                                } else if (! this.isLikeEmpty(lpFcnConfigCheckObj.numberUpperLimit)) { 
                                    if(parseInt(lpNumberToCheck) < lpFcnConfigCheckObj.numberUpperLimit) {
                                        this._adapter.log.warn('[Config Table \'' + functionConfigObj.tableName + '\'] Number in field "' + lpFcnConfigCheckObj.id + '" is greater than ' + lpFcnConfigCheckObj.numberUpperLimit  + ', which does not make sense.');
                                        if (lpFcnConfigCheckObj.deactivateIfError) lpTable.table[i].active = false;
                                        errors++;
                                        continue;
                                    }
                                }
                            // +++++++ VALIDATION TYPE: name (which can also be a drop down with multiple values) +++++++
                            } else if (lpFcnConfigCheckObj.type == 'name') {
                                let lpToCheck = lpTable.table[i][lpFcnConfigCheckObj.id];
                                
                                // Trim
                                if (Array.isArray(lpToCheck)) {
                                    for (let i = 0; i < lpToCheck.length; i++) {
                                        lpToCheck[i] = lpToCheck[i].trim();
                                    }
                                } else {
                                    lpToCheck = lpToCheck.trim();
                                }

                                // Handle forbidden state path chars
                                if (lpFcnConfigCheckObj.removeForbidden) {
                                    let checkForbiddenArray = [];
                                    if (Array.isArray(lpToCheck)) {
                                        checkForbiddenArray = lpToCheck; // we have an array (like pulldown menu)
                                    } else {
                                        checkForbiddenArray.push(lpToCheck); // we have a string
                                    }
                                    for (let i = 0; i < checkForbiddenArray.length; i++) {
                                        const forbidden = this.forbiddenCharsInStr(checkForbiddenArray[i], this._globals.forbiddenStatePaths);
                                        if (forbidden) {
                                            // We have forbidden state paths
                                            this.logExtendedInfo(`[Config Table '${functionConfigObj.tableName}'] Field value '${checkForbiddenArray[i]}' contains forbidden character(s) '${forbidden}', so we remove from string.`);
                                            if (Array.isArray(lpToCheck)) {
                                                lpToCheck[i] = lpToCheck[i].replace(this._globals.forbiddenStatePaths, '');
                                            } else {
                                                lpToCheck = lpToCheck.replace(this._globals.forbiddenStatePaths, '');
                                            }
                                        }    
                                    }
                                }

                                if (this.isLikeEmpty(lpToCheck) &&  !lpFcnConfigCheckObj.optional) {
                                    this._adapter.log.warn('[Config Table \'' + functionConfigObj.tableName + '\'] Field "' + lpFcnConfigCheckObj.id + '" is empty.');
                                    if (lpFcnConfigCheckObj.deactivateIfError) lpTable.table[i].active = false;
                                    errors++; 
                                    continue;
                                }
                                
                                // We altered lpToCheck, so set to adapter config.
                                lpTable.table[i][lpFcnConfigCheckObj.id] = lpToCheck;


                            // +++++++ VALIDATION TYPE: time and timeCron +++++++
                            // -- We test for both "time" and "timeCron".
                            } else if (lpFcnConfigCheckObj.type.substring(0, 4) == 'time') {
                                const lpToCheck = lpTable.table[i][lpFcnConfigCheckObj.id];
                                let isValidTime = false;
                                if (this.getTimeInfoFromAstroString(lpToCheck, false) ) {
                                    isValidTime = true;
                                } else if (lpFcnConfigCheckObj.type == 'timeCron' && this.isCronScheduleValid(lpToCheck.trim())) {
                                    isValidTime = true;
                                }
                                if (!isValidTime) {
                                    this._adapter.log.warn(`[Config Table '${functionConfigObj.tableName}'] No valid time in field '${lpFcnConfigCheckObj.id}': '${lpConfigString}'`);
                                    if (lpFcnConfigCheckObj.deactivateIfError) lpTable.table[i].active = false;
                                    errors++; 
                                    continue;
                                }


                            // +++++++ VALIDATION TYPE: overwrite +++++++
                            // -- for tableZones -> targetsOverwrite
                            } else if (lpFcnConfigCheckObj.type == 'overwrite') {

                                // Like: "targetsOverwrite": { "Bath.Light": "true", "Hallway Light": "50"}
                                const targetsOverwriteObject = this._adapter.config[functionConfigObj.tableId][i][lpFcnConfigCheckObj.id];
                                if (this.isLikeEmpty(targetsOverwriteObject)) continue; // nothing at this point
                                    
                                // loop thru the object to get the key name ("property")
                                for (const lpProperty in targetsOverwriteObject) {
                                    const name = lpProperty; // like 'Bath.Light'
                                    const lpStateVal        = targetsOverwriteObject[lpProperty];
                                    const lpConfigTableName = functionConfigObj.tableName;
                                    const lpStatePath = this.getOptionTableValue('tableTargetDevices', 'name', name, 'onState');
                                    
                                    
                                    if (!lpStatePath) {
                                        // This is actually handled already by verifying table Target Devices, so no error here.    
                                        continue;
                                        // throw(`Unexpected Error - Unable to retrieve state path by name '${name}' from tableTargetDevices.`);
                                    } 

                                    // Verify
                                    const stateValueValidationResult = await this.asyncVerifyConfig_verifyStateValue(lpStatePath, lpStateVal, true, lpConfigTableName);

                                    if (stateValueValidationResult.validationFailed) {
                                        if (lpFcnConfigCheckObj.deactivateIfError) lpTable.table[i].active = false;
                                        errors++; 
                                        continue;
                                    } else {
                                        this._adapter.config[functionConfigObj.tableId][i][lpFcnConfigCheckObj.id][name] = stateValueValidationResult.newStateVal;
                                    }
                                }
                            }
                        }
                        if(functionConfigObj.isTriggerTable) validTriggerCounter++;

                    }
                    let activeRowCounter = 0;
                    for (const lpRow of lpTable.table) {
                        if (lpRow.active) activeRowCounter++;
                    }

                    if (activeRowCounter == 0  && functionConfigObj.tableMustHaveActiveRows && !functionConfigObj.isTriggerTable) {
                        this._adapter.log.warn(`[Config Table '${functionConfigObj.tableName}'] No active rows defined (certain rows may have been deactivated due to errors, see previous logs for details).`);
                        errors++;
                    } 

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // We altered table variable, so set into adapter config
                    if (!(functionConfigObj.tableId == 'tableZoneExecution')) {
                        this._adapter.config[functionConfigObj.tableId] = lpTable.table;
                    } else {
                        // @ts-ignore - Type 'undefined' cannot be used as an index type
                        this._adapter.config.tableZones[lpTable.zoneTableRowNo].executionJson = JSON.stringify(lpTable.table);
                    }

                }

            }
            if (validTriggerCounter == 0) {
                this._adapter.log.warn('No active and valid trigger defined in any trigger table.');
                errors++;                    
            }



            // SECOND: Certain table values must be unique.
            // TODO: Add to g_tableValidation
            const uniqueCheckObjects = [
                // name: for logging only, column: the table to check, rows: the adapter config table rows
                { name:'Trigger Tables: Names',        column:'name',          rows:this._adapter.config.tableTriggerMotion.concat(this._adapter.config.tableTriggerDevices, this._adapter.config.tableTriggerTimes) },
                { name:'Target device table: Names',                column:'name',    rows:this._adapter.config.tableTargetDevices },
                { name:'Zones table: Names',                    column:'name',      rows:this._adapter.config.tableZones },
                { name:'Conditions table: Names',                   column:'name', rows:this._adapter.config.tableConditions },
                // { name:'Trigger Tables Motion/Other: State Paths',  column:'stateId',       rows:this._adapter.config.tableTriggerMotion.concat(this._adapter.config.tableTriggerDevices) },
            ];
            for (const lpCheckObj of uniqueCheckObjects) {
                if (!this.isLikeEmpty(lpCheckObj.rows)) {
                    const allValues = [];
                    for (const lpRow of lpCheckObj.rows) {
                        if(lpRow.active) {
                            allValues.push(lpRow[lpCheckObj.column]);
                        }
                    }
                    if ( !this.isArrayUnique(allValues) ) {
                        this._adapter.log.error(`${lpCheckObj.name} must be unique. You cannot use same string more than once here.`);
                        errors++;
                    }
                }
            }


            // FINALIZE
            if (errors == 0) {
                return true;
            } else {
                this._adapter.log.error(`[User Error (Anwenderfehler)] - ${errors} error(s) found in adapter configuration --> Please check the previous warn log for details and then correct your configuration accordingly. You must fix these issue(s) to use this adapter.`);
                return false;
            }

        } catch (error) {
            this.dumpError('[asyncVerifyConfig()]', error);
            return false;
        }
    }

    /**
     * Verifies a state value
     * 
     * @param {string} statePath
     * @param {*} stateVal
     * @param {boolean} isOptional 
     * @param {string} configTableName -- name of config table, for log only
     * @return {Promise<object>}   {validationFailed:false, newStateVal:'xyz'}
     */
    async asyncVerifyConfig_verifyStateValue(statePath, stateVal, isOptional, configTableName) {

        try {

            // Remove any brackets at beginning and end
            stateVal = stateVal.replace(/^(\[|"|'|`|´)([\s\S]*)(]|"|'|`|´)$/, '$2');

            // Trim again
            stateVal = stateVal.trim();

            if ( this.isLikeEmpty(statePath.trim()) || (this.isLikeEmpty(stateVal) && isOptional) ) {
                // Skip if empty and if is optional. 
                // We also check if the state path is empty. 
                return {validationFailed:false, newStateVal:''};
            }

            const lpStateObject = await this._adapter.getForeignObjectAsync(statePath);
            
            if (!lpStateObject) {
                // State does not exist
                this._adapter.log.warn(`[Config Table '${configTableName}'] State '${statePath}' does not exist.`);
                return {validationFailed:true};
            } else {
                // State exists
                // Verify State Type (like boolean, switch, number)
                const stateType = lpStateObject.common.type;

                if (stateType == 'boolean' || stateType == 'switch') {
                    if (stateVal != 'true' && stateVal != 'false') {
                        this._adapter.log.warn(`[Config Table '${configTableName}'] State '${statePath}' is expecting boolean (true/false), but you set '${stateVal}'.`);
                        return {validationFailed:true};
                    } else {
                        if (stateVal == 'true') stateVal = true;
                        if (stateVal == 'false') stateVal = false;
                        return {validationFailed:false, newStateVal:stateVal};
                    }
                } else if (stateType == 'number') {
                    // We allow comparators '<', '>=' etc.
                    const isComparator = (/^(>=|<=|>|<)\s?(\d{1,})$/.test(stateVal.trim()));
                    if (isComparator) {
                        return {validationFailed:false, newStateVal:stateVal};
                    } else if (this.isNumber(stateVal)) {
                        return {validationFailed:false, newStateVal:parseFloat(stateVal)};
                    } else {
                        this._adapter.log.warn(`[Config Table ${configTableName}] State ${statePath} is expecting a number, but you set '${stateVal}'.`);
                        return {validationFailed:true};
                    }
                } else if (this.isLikeEmpty(stateVal)) {
                    // Let's convert an "like empty" value to an empty string, just to make sure....
                    return {validationFailed:false, newStateVal:''};
                } else {
                    // Nothing to do at this point; return 
                    return {validationFailed:false};
                }
            }            
        } catch (error) {
            this.dumpError('[verifyStateValue()]', error);
            return {validationFailed:true};
        }

    }


    /**
     * Create smartcontrol.x.info.astroTimes states.
     * 
     * @return {Promise<boolean>}   true if successful, false if not.
     */
    async asyncCreateAstroStates() {

        try {
            const statesToBeCreated = [];

            for (let i = 0; i < this._globals.astroTimes.length; i++) {
                const lpAstro = this._globals.astroTimes[i];
                const statePathMain = `${this._adapter.namespace}.info.astroTimes.${this.zeroPad(i+1, 2)}_${lpAstro}`;
                const commonMain = { name: `${lpAstro} – ${this._globals.astroTimesGerman[i]}`, type: 'string', read: true, write: false, role: 'value', def: '' };
                statesToBeCreated.push({statePath:statePathMain, commonObject:commonMain});

                // Also, create timestamps
                const statePathTs = `${this._adapter.namespace}.info.astroTimes.timeStamps.${lpAstro}`;
                const commonTs = { name: `${lpAstro} – ${this._globals.astroTimesGerman[i]}`, type: 'number', read: true, write: false, role: 'value', def: 0 };
                statesToBeCreated.push({statePath:statePathTs, commonObject:commonTs});


            }

            // Create all states
            const createStatesResult = await this.asyncCreateStates(statesToBeCreated);
            if (!createStatesResult) throw (`Certain error(s) occurred in asyncCreateStates().`);

            // Set current astro values
            await this.refreshAstroStatesAsync();

            return true;

        } catch (error) {
            this.dumpError('[asyncCreateAstroStates()]', error);
            return false;
        }        
    }

    /**
     * Refresh the astro states under smartcontrol.x.info.astroTimes
     */
    async refreshAstroStatesAsync() {

        try {

            for (let i = 0; i < this._globals.astroTimes.length; i++) {
                const lpAstro = this._globals.astroTimes[i];

                const ts = this.getAstroNameTs(lpAstro, this.latitude, this.longitude);
                const astroTimeStr = this.timestampToTimeString(ts, true);
                await this._adapter.setStateAsync(`info.astroTimes.${this.zeroPad(i+1, 2)}_${lpAstro}`, {val: astroTimeStr, ack: true });
                
                await this._adapter.setStateAsync(`info.astroTimes.timeStamps.${lpAstro}`, {val: ts, ack: true });

            }

        } catch (error) {
            this.dumpError('[refreshAstroStatesAsync()]', error);
            return false;            
        }

    }


    /**
     * Create smartcontrol.x.targetDevices.xxx states and delete no longer needed ones.
     * 
     * @return {Promise<boolean>}   true if successful, false if not.
     */
    async asyncCreateTargetDevicesStates() {

        try {

            /*********************************
             * A: Get all states to be created.
             *********************************/            
            const statesToBeCreated = [];
            const statePaths = [];
            for (const lpRow of this._adapter.config.tableTargetDevices) {
                
                if (!lpRow.active) continue;
                const lpStatePath = `${this._adapter.namespace}.targetDevices.${lpRow.name.trim()}`;
                if (! this.isStateIdValid(lpStatePath) ) throw(`Invalid state name portion provided in table 'Target Devices': '${lpRow.name}'`);

                const lpCommon = { name: lpRow.name, type: 'boolean', read: true, write: true, role: 'switch', def: false };
                statesToBeCreated.push({statePath:lpStatePath, commonObject:lpCommon});
                statePaths.push(lpStatePath);
            }

            /*********************************
             * B: Create all states
             *********************************/

            const createStatesResult = await this.asyncCreateStates(statesToBeCreated);
            if (!createStatesResult) throw (`Certain error(s) occurred in asyncCreateStates().`);

            /*********************************
             * B: Delete all states which are no longer used.
             *********************************/
            const allTargetDevicesStates = await this._adapter.getStatesOfAsync('targetDevices');
            if (allTargetDevicesStates == undefined) throw (`getStatesOfAsync(): Could not get adapter instance state paths for 'targetDevices'.`);

            for (const lpState of allTargetDevicesStates) {
                const statePath = lpState._id; // like: 'smartcontrol.0.targetDevices.Coffeemaker'
                if ( statePaths.indexOf(statePath) == -1 ) {
                    // State is no longer used.
                    await this._adapter.delObjectAsync(statePath); // Delete state.                
                    this._adapter.log.debug(`State '${statePath}' deleted, since option does no longer exist.'`);
                }
            }

            return true;

        } catch (error) {
            this.dumpError('[asyncCreateTargetDevicesStates()]', error);
            return false;
        }        
    }

    /**
     * Create smartcontrol.x.userstates states.
     * Also, delete states no longer used.
     * 
     * @return {Promise<boolean>}   true if successful, false if not.
     */
    async asyncCreateUserTriggerStates() {

        try {
            const statePathsUsed = []; // unused will be deleted.            
            for (const lpRow of this._adapter.config.tableTriggerDevices) {
                const statePath = `userstates.${lpRow.stateId}`;
                const statePathFull = `${this._adapter.namespace}.${statePath}`;
                if(lpRow.userState) { // We also create states for inactive rows, since user likely deactivates temporarily.
                    if (this.isStateIdValid(`${this._adapter.namespace}.${statePath}`)) {
    
                        const stateCommonString =  {name:`User trigger '${lpRow.name}' of 'Other Devices' in Zones table`, type:'string', read:true, write:true, role:'state', def:'' };
                        const stateCommonBoolean = {name:`User trigger '${lpRow.name}' of 'Other Devices' in Zones table`, type:'boolean', read:true, write:true, role:'state', def:false };

                        const lpStateObject = await this._adapter.getObjectAsync(statePath);
                        if (!lpStateObject) {
                            // State does not exist
                            let stateObj = {};
                            if(lpRow.stateVal=='true' || lpRow.stateVal=='false') {
                                stateObj = {statePath:statePath, commonObject:stateCommonBoolean};
                            } else {
                                stateObj = {statePath:statePath, commonObject:stateCommonString};
                            }
                            await this.asyncCreateStates(stateObj, false); // Create state
                            this._adapter.log.debug(`User state '${statePathFull}' created per option table 'Other Devices'.`);
                        } else {
                            // State exists. Now let's check if the "type" changed from boolean to string or vice versa.
                            const isStateConfValBoolean = (lpRow.stateVal=='true' || lpRow.stateVal=='false') ? true : false;
                            const isStateRealValBoolean = (lpStateObject.common.type == 'boolean' || lpStateObject.common.type == 'switch') ? true : false;
                            if (isStateConfValBoolean != isStateRealValBoolean) {
                                const newCommon = (isStateConfValBoolean) ? stateCommonBoolean : stateCommonString;
                                this._adapter.log.debug(`User state '${statePathFull}' - state type changed, so delete old state and create new one.`);
                                await this._adapter.delObjectAsync(statePath); // Delete state. 
                                await this.asyncCreateStates({statePath:statePath, commonObject:newCommon}, false);
                            }
                        }
                        statePathsUsed.push(statePathFull);
                    } else {
                        throw(`State path '${this._adapter.namespace}.${statePath}' is not valid.`);
                    }
                }
            }
            // Delete states no longer needed
            const allUserStates = await this._adapter.getStatesOfAsync('userstates');
            if (allUserStates == undefined) throw (`Could not get adapter instance state paths for 'userstates'.`);
            
            for (const lpState of allUserStates) {
                const existingStatePath = lpState._id; // like: 'smartcontrol.0.userstates.Coffeemaker'
                if ( statePathsUsed.indexOf(existingStatePath) == -1 ) {
                    // State is no longer used.
                    await this._adapter.delObjectAsync(existingStatePath); // Delete state.                
                    this._adapter.log.debug(`State '${existingStatePath}' deleted, since trigger does no longer exist.'`);
                }
            }            
            return true;

        } catch (error) {
            this.dumpError('[asyncCreateUserTriggerStates()]', error);
            return false;
        }   


    }


    /**
     * Create option states and delete states no longer used.
     * 
     * @return {Promise<boolean>}   true if successful, false if not.
     */
    async asyncCreateOptionStates() {
        try {
        
            const statesFull = this.generateOptionStates(false, true);
            const statePathsOnly = this.generateOptionStates(false, false);

            /*********************************
             * B: Create all states
             *********************************/

            const res = await this.asyncCreateStates(statesFull, false);
            if (!res) {
                throw(`Certain error(s) occurred in asyncCreateStates().`);
            }

            /*********************************
             * B: Delete all states which are no longer used.
             *********************************/
            const allAdapterStates = await this._adapter.getStatesOfAsync('options');
            if (allAdapterStates != undefined) {
                for (const lpState of allAdapterStates) {
                    const statePath = lpState._id; // like: 'smartcontrol.0.options.Zones.Hallway.name'
                    if ( (statePathsOnly.indexOf(statePath) == -1) && (statePath.endsWith('active') || (statePath.endsWith('name') ) ) ) {
                        // State is no longer used.
                        await this._adapter.delObjectAsync(statePath); // Delete state.                
                        this.logExtendedInfo(`State '${statePath}' deleted, since option does no longer exist.'`);
                    }
                }
            }
            return true;

        } catch (error) {
            this.dumpError('[asyncCreateOptionStates()]', error);
            return false;
        }        
    }


    /**
     * Generate option states
     * 
     * @param {boolean}  all   If true, all options will be created as states, if false: limited to 'active' and 'name'
     * @param {boolean}  [withCommon=false]    If true, with common objects for state creation, false: just the state paths
     * @return {array}   Array of all states that were created.
     * 
     */
    generateOptionStates(all, withCommon=false) {

        const optionsStatesResult = [];
        const tablesToProcess = [
            'tableTriggerMotion',
            'tableTriggerDevices',
            'tableTriggerTimes',
            'tableTargetDevices',
            'tableZones',
            'tableConditions',
        ];
        let errorCounter = 0;
        for (let i = 0; i < tablesToProcess.length; i++) {

            for (let k = 0; k < this._adapter.config[tablesToProcess[i]].length; k++) {
            
                const lpRow = this._adapter.config[tablesToProcess[i]][k];
            
                // Get table name for the state portion 
                let lpStateSubSection = tablesToProcess[i]; // Table name from config, like 'tableTargetDevices';
                lpStateSubSection = lpStateSubSection.substr(5); // 'tableTargetDevices' => 'TargetDevices'

                // Get the name of the table row and convert to a valid state portion.
                const lpRowNameStatePortion = lpRow.name;  // like: 'Motion Bathroom' or 'At 04:05 every Sunday'
                if (this.isLikeEmpty(lpRow.name.trim())) {
                    // We do not add rows with blank name
                    continue;
                }

                // This is already done in asyncVerifyConfig!
                /*
                lpRowNameStatePortion = lpRowNameStatePortion.replace(this._globals.forbiddenStatePaths, '');
                if (this.isLikeEmpty(lpRowNameStatePortion)) {
                    this._adapter.log.debug(`[${tablesToProcess[i]}] Name string cannot be converted to state string portion since less than 3 chars (after removing forbidden chars): [${lpRowNameStatePortion}]`);
                    errorCounter++;
                    continue;
                }
                */

                // for...in to loop through the object (table row)
                for (const fieldName in lpRow){

                    const fieldEntry = lpRow[fieldName]; // like 'smartcontrol.0.Test.light.Bathroom'


                    if(!all && (fieldName != 'active' && fieldName != 'name')) {
                        // If all=false, ignore if not 'active' or 'name'
                        continue;
                    }
                    
                    // We always need the name


                    const lpCommonObject = {};
                    if(withCommon) {

                        // Define the common object to create the state
                        lpCommonObject.name = (fieldName != 'active') ? fieldName : 'Please note: Changing this state restarts the adapter instance to put the change into effect';
                        lpCommonObject.read = true;
                        lpCommonObject.write = true;
                        lpCommonObject.role = 'value';

                        /**
                         * * We convert all all to string, except to 'active'.
                         * * Reason: User input in adapter settings is string as well. And so Users can change a state from 'true' to like 'Radio is playing'.
                         */
                        if (fieldName == 'active') {
                            // active check box of table row should always be boolean and not string.
                            lpCommonObject.type = 'boolean';
                            lpCommonObject.def  = fieldEntry;                            
                        } else {
                            // all others
                            lpCommonObject.type = 'string';
                            lpCommonObject.def  = (typeof fieldEntry != 'string') ? JSON.stringify(fieldEntry) : fieldEntry;
                        }

                        // Don't allow to change the 'name'
                        if (fieldName == 'name') {
                            lpCommonObject.write = false;
                        }

                    }

                    const lpStatePath = `${this._adapter.namespace}.options.${lpStateSubSection}.${lpRowNameStatePortion}.${fieldName}`; // Like: 'options.TargetDevices.Bathroom Light'
                    if (! this.isStateIdValid(`${this._adapter.namespace}.${lpStatePath}`) ) {
                        this._adapter.log.error(`[${tablesToProcess[i]}] We were not able to generate a valid state path. This is what was determined to be not valid: [${lpStatePath}].`);
                        errorCounter++;
                        continue;
                    }

                    if (withCommon) {
                        optionsStatesResult.push({statePath:lpStatePath, commonObject:lpCommonObject });
                    } else {
                        optionsStatesResult.push(lpStatePath);
                    }
                    
                }
                
            }

        }

        if (errorCounter > 0) {
            this._adapter.log.error(`${errorCounter} error(s) occurred while processing state generation of options.`);
            return [];
        } else if (optionsStatesResult.length == 0) {
            this._adapter.log.error(`No states to be created determined while processing state generation of options.`);
            return [];
        } else {
            return optionsStatesResult;
        }

    }





    /**
     * Schedule all trigger times of Trigger Times table
     * 
     * @return {number}   number of schedules activated.
     */
    scheduleTriggerTimes() {

        try {
            
            let counter = 0;
            for (const lpRow of this._adapter.config.tableTriggerTimes) {
                if (lpRow.active) {

                    // Convert non-crons to cron
                    let cron = '';
                    if (this.isCronScheduleValid(lpRow.time.trim())) {
                        cron = lpRow.time.trim();
                    } else {
                        const ts = this.getTimeInfoFromAstroString(lpRow.time, true).timestamp;
                        if (ts == 0) {
                            this._adapter.log.warn(`No valid time in Trigger Times table, row ${lpRow.name}: ${lpRow.time}`);
                            continue;
                        }
                        const date = new Date(ts);
                        cron = `${date.getMinutes()} ${date.getHours()} * * *`;
                    }

                    this._schedules[lpRow.name] = this._Schedule.scheduleJob(cron, async () => {

                        const cP = await this.getTriggerConfigParamAsync(lpRow.name);
                        if(cP && cP.triggerName) {

                            // First check if additional conditions are met or "never if"
                            let doContinue = await this.asyncAreScheduleConditionsMet(cP, cP.triggerTmAdditionCond, true);
                            if (doContinue) doContinue = await this.asyncAreScheduleConditionsMet(cP, cP.triggerTmNever, false, true);
                            if(!doContinue) {
                                this.logExtendedInfo(`Execution table row ${cP.triggerName} (time: ${cP.triggerTime}) triggered, but condition(s) not met.`);
                                return;
                            }
                            this._adapter.log.debug(`Execution table row ${cP.triggerName} (time: ${cP.triggerTime}) triggered.`);
                            this.switchTargetsExecuteAsync(cP);
                        }

                    });
                    counter++;

                }
            }
            return counter;

        } catch (error) {
            this.dumpError('[scheduleTriggerTimes]', error);
            return 0;
        }    

    }

    /***
     *     ██████  ███████ ███    ██ ███████ ██████  ██  ██████      █████  ██████   █████  ██████  ████████ ███████ ██████           ██ ███████ 
     *    ██       ██      ████   ██ ██      ██   ██ ██ ██          ██   ██ ██   ██ ██   ██ ██   ██    ██    ██      ██   ██          ██ ██      
     *    ██   ███ █████   ██ ██  ██ █████   ██████  ██ ██          ███████ ██   ██ ███████ ██████     ██    █████   ██████           ██ ███████ 
     *    ██    ██ ██      ██  ██ ██ ██      ██   ██ ██ ██          ██   ██ ██   ██ ██   ██ ██         ██    ██      ██   ██     ██   ██      ██ 
     *     ██████  ███████ ██   ████ ███████ ██   ██ ██  ██████     ██   ██ ██████  ██   ██ ██         ██    ███████ ██   ██      █████  ███████ 
     *
     *    Generic Adapter functions - tailored to this adapter
     */

    /**
     * Verify acknowledge (ack) once a state was changed.
     * 
     * In General: Any state changes of states will be ignored, if acknowledge (ack) = false.
     * The reason is that adapters confirm states by the acknowledge "ack" flag (setting to true).
     * Reference: https://forum.iobroker.net/post/448606
     * 
     * Exception 1: States under javascript.x/0_userdata.0/alias.x: For states created by users, this behavior can be changed in the adapter options.
     * Exception 2: States under smartcontrol.x.: ack:false only.
     * Exception 3: States which are not under a "real" adapter namespace, so like "Messwerte.0". Reason: Several users have created 
     *              own states in the object main tree, like "Messwerte.0". https://forum.iobroker.net/post/461189
     * 
     * @param {string}                          statePath   - State Path
     * @param {ioBroker.State|null|undefined}   stateObject - State object
     * @return {Promise<object>}     {passing: false; msg:'some message'}
     *                               if state change shall be ignored due to the ack (or error occurred), otherwise true.
     * 
     */
    async isAckPassing(statePath, stateObject) {

        try {

            if (!stateObject || !statePath) 
                return {passing:false, msg:`Unexpected error: isAckPassing() was no valid statePath or stateObject provided.`};

            const namespace = `${statePath.split('.')[0]}.${statePath.split('.')[1]}`; // like sonos.1

            // For any states under this adapter instance (so )smartcontrol.x), we require ack = false;
            // 22-Jul-2020: we allow different adapter instances as well.
            // if (statePath.startsWith(`${this._adapter.namespace}.`)) {
            if (statePath.match(/^smartcontrol.\d+./)) {
                if (stateObject.ack == false) {
                    return {passing:true, msg:`Adapter Instance State (${this._adapter.namespace}) requires ack:false`};
                } else {
                    return {passing:false, msg:`Adapter Instance State (${this._adapter.namespace}) requires ack:false`};
                }
            }

            // Handle User States
            let isUserState = false;
            if (statePath.startsWith('javascript.') || statePath.startsWith('0_userdata.0') || statePath.startsWith('alias.')) isUserState = true;

            if (!isUserState) {
                // Check if state is under a "real" adapter namespace.
                const isRealAdapter = await this._adapter.getForeignObjectAsync(`system.adapter.${namespace}`);
                if (!isRealAdapter) isUserState = true;
            }

            if (isUserState) {

                if (!this._adapter.config.triggerStatesAck || this._adapter.config.triggerStatesAck == 'false') {
                    if (stateObject.ack == false) {
                        return {passing:true, msg:`User State (${statePath}) requires ack:false per adapter configuration`};
                    } else {
                        return {passing:false, msg:`User State (${statePath}) requires ack:false per adapter configuration`};
                    }
            
                } else if (this._adapter.config.triggerStatesAck == 'true') {
                    if (stateObject.ack == true) {
                        return {passing:true, msg:`User State (${statePath}) requires ack:true per adapter configuration`};
                    } else {
                        return {passing:false, msg:`User State (${statePath}) requires ack:true per adapter configuration`};
                    }
                } else {
                    // any (ack: true or false)
                    return {passing:true, msg:`User State (${statePath}) can be ack:true or ack:false per adapter configuration`};
                }

            } else {
                // For any other "real" adapter state changes, we require ack = true
                if (stateObject.ack == true) {
                    return {passing:true, msg:`"Real" adapter state (${statePath}) requires ack:true`};
                } else {
                    return {passing:false, msg:`"Real" adapter state (${statePath}) requires ack:true`};
                }
            }
        
        } catch (error) {
            this.dumpError('[isAckPassing()]', error);
            return {passing:false, msg:`Unexpected error occurred in isAckPassing()`};
        }

    }


    /**
     * Stop all timers
     */
    stopAllTimers() {
        // loop through objects by for...in
        for (const timerType in this._timers) {
            // timerType is e.g. 'motion'
            for (const timerName in this._timers[timerType]) {
                this._adapter.log.debug('Stopping timer: ' + timerName);
                clearTimeout(this._timers[timerType][timerName]);
                this._timers[timerType][timerName] = null;
            }
        }   
    }

    /**
     * Stop all schedules
     */
    stopAllSchedules() {
        // loop through objects by for...in
        let counter = 0;
        for (const lpScheduleName in this._schedules) {
            this._adapter.log.debug('Cancelling schedule: ' + lpScheduleName);
            this._schedules[lpScheduleName].cancel();
            counter++;
        }
        this._adapter.log.debug(`(${counter}) trigger schedules cancelled...`);

    }    



    /**
     * Checks if a cron string like xxx is valid or not.
     * Source:  "Test if schedule is valid" - https://github.com/node-schedule/node-schedule/issues/296#issuecomment-269971152
     * Read:    https://stackoverflow.com/questions/35817080/node-requiring-module-inside-function
     * 
     * @param {string} str  String to check, so like '0 22 * * 1-5'
     * @return {boolean}  true if we have a valid cron string, false if not.
     */
    isCronScheduleValid(str) {

        // node-schedule uses cron-parser, so we can simply require it here without adding to package.json
        const cronParser = require('cron-parser');
        try {
            cronParser.parseExpression(str);
        } catch (error) {
            // handle the parse error
            return false;
        }
        // success
        return true;
            
    }


    /**
     * Logs information to ioBroker log.
     * If "extendedInfoLog" in adapter settings is disabled, log level is debug.
     * @param {string}   msg    The log message
     */
    logExtendedInfo(msg) {
        if(this._adapter.config.extendedInfoLog) {
            this._adapter.log.info(msg);
        } else {
            this._adapter.log.debug(msg);
        }
    }

    /**
     * Retrieves a value of an admin option table
     * 
     * Call example: const res = this.getOptionTableValue('tableTargetDevices', 'name', 'Bathroom Light', 'offState');
     * 
     * @param {string}  tableId          The id of the table, like 'tableTargetDevices'
     * @param {string}  identifierId     The column id of the identifier, like 'name'
     * @param {string}  identifierName   Like 'Bathroom Light'
     * @param {string}  resultId         The column id of the value we need, like 'offState'
     * @return {*}                       The value, or undefined if nothing found.
     */
    getOptionTableValue(tableId, identifierId, identifierName, resultId) {
        for (const tableRow of this._adapter.config[tableId]) {
            if (tableRow.active) {
                if (tableRow[identifierId] == identifierName) {
                    // We got a hit
                    return tableRow[resultId];
                } else {
                    // no hit
                    continue;
                }
            } else {
                //this._adapter.log.warn(`[getOptionTableValue] Config Table '${tableId}', row '${identifierName}' is not active.`);
                continue;
            }
        }
        // Nothing found, return undefined
        return undefined;
    }



    /**
     * Check if current day is within the mon-sun options as set in Execution table.
     * 
     * @param {object}  row   The "Execution" table row which was triggered.
     * @param {number}  ts    The timestamp to check.
     * @param {string} ofWhat Typically zone name. For logging purposes only.
     * @return {boolean}      True if matching, false if not.
     */
    scheduleIsWeekdayMatching(row, ts, ofWhat) {
        const days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
        const lpWeekdayGiven = getWeekDayString(ts);
        for (const lpDay of days) {
            if (lpWeekdayGiven == null) {
                this._adapter.log.error(`[scheduleIsWeekdayMatching()] Unable to calculate day of week for given timestamp '${ts}'`);
                return false;
            } else if ( (row[lpDay] == true) && (lpDay == lpWeekdayGiven) ) {
                this._adapter.log.debug(`${ofWhat} - Current weekday '${lpWeekdayGiven}' *is* matching Execution table row.`);
                return true;
            }
        }        
        // There was no hit, so we return false.
        this._adapter.log.debug(`${ofWhat} - Current weekday '${lpWeekdayGiven}' is *not* matching Execution table row.`);
        return false;

        /**
         * get the week day of a string.
         * Source: https://stackoverflow.com/a/17964373
         * @param {*} date object or time stamp or string that is recognized by the Date.parse() method
         * @return {string|null}   day of week as string, or null if not found.
         */
        function getWeekDayString(date) {
            const dayOfWeek = new Date(date).getDay();    
            return isNaN(dayOfWeek) ? null : ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'][dayOfWeek];
        }

    }


    /**
     * Check if a timestamp is within two times.
     *
     * @param {number} tsGiven  Timestamp to be checked if between start and end time
     * @param {string} start    Start time, like '18:30', or 'sunrise, or 'sunset+30'
     * @param {string} end      End time
     * @param {string} ofWhat   Typically zone name. For logging purposes only.
     * @return {boolean}        true if within times, and false if not.
     */
    scheduleIsTimeStampWithinPeriod(tsGiven, start, end, ofWhat) {

        try {
    
            // First: convert potential Astro start time to "normal", so like 'sunset-30' to '18:30'
            const givenFinal = this.timestampToTimeString(tsGiven, true);
            const startFinal = this.getTimeInfoFromAstroString(start, true).hoursMins;
            const endFinal   = this.getTimeInfoFromAstroString(end, true).hoursMins;

            if (!givenFinal) throw(`givenFinal is undefined. start: '${start}', end: '${end}', ofWhat: '${ofWhat}'`);
            if (!startFinal) throw(`startFinal is undefined. start: '${start}', end: '${end}', ofWhat: '${ofWhat}'`);
            if (!endFinal) throw(`endFinal is undefined. start: '${start}', end: '${end}', ofWhat: '${ofWhat}'`);

            // Next, check if given time is in between
            if (this.isTimeBetween(startFinal, endFinal, givenFinal)) {
                this._adapter.log.debug(`'${ofWhat}' - Current time '${givenFinal}' *is* within schedule times (start: '${startFinal}', end: '${endFinal}).`);
                return true;                
            } else {
                this._adapter.log.debug(`'${ofWhat}' - Current time '${givenFinal}' is *not* within schedule times (start: '${startFinal}', end: '${endFinal}).`);
                return false;                                
            }

        } catch (error) {
            this.dumpError('[scheduleIsTimeStampWithinPeriod]', error);
            return false;
        }




    }

    /**
     * Get the timestamp of an astro name.
     * 
     * @param {string} astroName            Name of sunlight time, like "sunrise", "nauticalDusk", etc.
     * @param {number} latitude             Latitude
     * @param {number} longitude            Longitude
     * @param {number} [offsetMinutes=0]    Offset in minutes
     * @return {number}                     Timestamp of the astro name
     */
    getAstroNameTs(astroName, latitude, longitude, offsetMinutes=0) {

        try {
            let ts = this._sunCalc.getTimes(new Date(), latitude, longitude)[astroName];
            if (!ts || ts.getTime().toString() === 'NaN') {
                // Fix night / nightEnd per adapter options.
                // In northern areas is no night/nightEnd provided in the Summer. 
                // So we use 0:00 for night and 2:00 for night end as fallback.
                if (this._adapter.config.fixNightNightEnd && ['night', 'nightEnd'].includes(astroName) ) {
                    const currDate = new Date(); 
                    const midnightTs = currDate.setHours(24,0,0,0); // is the future midnight of today, not the one that was at 0:00 today
                    switch (astroName) {
                        case 'night':
                            this._adapter.log.debug(`[getAstroNameTs] No time found for [${astroName}], so we set 00:00 as fallback per adapter config.`);
                            return midnightTs; // midnight - 00:00
                        case 'nightEnd': 
                            this._adapter.log.debug(`[getAstroNameTs] No time found for [${astroName}], so we set 02:00 as fallback per adapter config.`);
                            return midnightTs + (1000*3600*2); // 02:00
                    }
                } else {
                    this._adapter.log.warn(`[getAstroNameTs] No time found for [${astroName}].`);
                    return 0;
                }
            }

            ts = this.roundTimeStampToNearestMinute(ts);
            ts = ts + (offsetMinutes * 60 * 1000);
            return ts;
        } catch (error) {
            this.dumpError('[getAstroNameTs()]', error);
            return 0;
        }

    }


    /**
     * Get the time stamp of a time string, like '23:30', 'goldenHourEnd', or 'goldenHourEnd+30'
     * Supports times like 'h:ss'/'hh:ss', and also astro names.
     * Astro names can also have an additional offset in minutes, so like "sunriseEnd-40" or "sunriseEnd+120".
     * 
     * @param {string}   input          the input string
     * @param {boolean}  [getFullResult=false]  if false: returns true if a valid time provided, and false if not.
     *                                          if true: Empty object if nothing found, or object with details.
     * @return {boolean|object}                 Return value
     */
    getTimeInfoFromAstroString(input, getFullResult=false) {

        const currentDateTime = new Date(); // Date object of current date/time

        input = input.replace(/\s/g,''); // remove all white spaces

        const returnObject = {
            full: input,
            timestamp: 0,
            hoursMins: '', // like '19:35'
            isAstro: false,
            astroName: '',
            astroHasOffset: false,
            astroOffsetMinutes: 0,
        };

        const matchTime = input.match(/^(\d{1,2})(:)(\d{2})$/);
        const matchAstroName = input.match(/^(sunriseEnd|sunrise|goldenHourEnd|solarNoon|goldenHour|sunsetStart|sunset|dusk|nauticalDusk|nightEnd|nadir|night|nauticalDawn|dawn)$/g);
        const matchAstroWithOffset = input.match(/^(sunriseEnd|sunrise|goldenHourEnd|solarNoon|goldenHour|sunsetStart|sunset|dusk|nauticalDusk|nightEnd|nadir|night|nauticalDawn|dawn)(-|\+)(\d{1,3})$/);

        if (matchTime != null) {
            // No Astro.
            // Time like "19:35" found. Convert to timestamp by using current date
            
            const hour = parseInt(matchTime[1]);
            const min = parseInt(matchTime[3]);

            if ( (input != '24:00') && (hour < 0 || hour > 23 || min < 0 || min > 60 ) ) {
                returnObject.full = '';
            } else {

                returnObject.timestamp = new Date(
                    currentDateTime.getFullYear(),
                    currentDateTime.getMonth(),
                    currentDateTime.getDate(),
                    hour, // hours
                    min, // minutes
                ).getTime();

            }
        } else {
            if (matchAstroName != null) {
                // Astro found, with no offset
                returnObject.astroName = input;
                returnObject.isAstro = true;
                returnObject.timestamp = this.getAstroNameTs(input, this.latitude, this.longitude);
            } else if (matchAstroWithOffset != null) {
                // Astro found, with offset
                returnObject.isAstro = true;
                returnObject.astroHasOffset = true;
                returnObject.astroName = matchAstroWithOffset[1];
                returnObject.astroOffsetMinutes = parseInt(matchAstroWithOffset[3]);
                if(matchAstroWithOffset[2] == '-') {
                    returnObject.astroOffsetMinutes = returnObject.astroOffsetMinutes * (-1);
                }
                returnObject.timestamp = this.getAstroNameTs(returnObject.astroName, this.latitude, this.longitude, returnObject.astroOffsetMinutes);
            } else {
                // Nothing found
                returnObject.full = '';
            }
        }

        // Add hourMins string
        returnObject.hoursMins = this.timestampToTimeString(returnObject.timestamp, true);

        // handle '24:00'. We add a full day.
        if (input == '24:00') {
            returnObject.timestamp = returnObject.timestamp + +1000*60*60*24;
            returnObject.hoursMins = '24:00';
        }


        if (!getFullResult) {
            if (!returnObject.full) {
                return false;
            } else {
                return true;
            }
        } else {
            return returnObject;
        }

    }


    /***
     *     ██████  ███████ ███    ██ ███████ ██████  ██  ██████      █████  ██████   █████  ██████  ████████ ███████ ██████           ██ ███████ 
     *    ██       ██      ████   ██ ██      ██   ██ ██ ██          ██   ██ ██   ██ ██   ██ ██   ██    ██    ██      ██   ██          ██ ██      
     *    ██   ███ █████   ██ ██  ██ █████   ██████  ██ ██          ███████ ██   ██ ███████ ██████     ██    █████   ██████           ██ ███████ 
     *    ██    ██ ██      ██  ██ ██ ██      ██   ██ ██ ██          ██   ██ ██   ██ ██   ██ ██         ██    ██      ██   ██     ██   ██      ██ 
     *     ██████  ███████ ██   ████ ███████ ██   ██ ██  ██████     ██   ██ ██████  ██   ██ ██         ██    ███████ ██   ██      █████  ███████ 
     *
     *    Generic Adapter functions - independent from this adapter
     */


    /**
     * Error Message to Log. Handles error object being provided.
     * 
     * @param {string} msg               (intro) message of the error
     * @param {*}      [error=undefined]  Optional: Error object or string
     */
    dumpError(msg, error=undefined) {
        if (!error) {
            this._adapter.log.error(msg);
        } else {
            if (typeof error === 'object') {
                if (error.stack) {
                    this._adapter.log.error(`${msg} – ${error.stack}`);
                } else if (error.message) {
                    this._adapter.log.error(`${msg} – ${error.message}`);
                } else {
                    this._adapter.log.error(`${msg} – ${JSON.stringify(error)}`);
                }
            } else if (typeof error === 'string') {
                this._adapter.log.error(`${msg} – ${error}`);
            } else {
                this._adapter.log.error(`[dumpError()] : wrong error argument: ${JSON.stringify(error)}`);
            }
        }
    }


    /**
     * Restart the adapter instance.
     * Source: https://github.com/foxriver76/ioBroker.xbox/blob/275e03635d657e7b18762166b4feca96fc4b1b1c/main.js#L630
     */
    /*
    async asyncRestartAdapter() {
        
        try {
        
            const resultObject = await this._adapter.getForeignObjectAsync('system.adapter.' + this._adapter.namespace);
            if (resultObject) {
                await this._adapter.setForeignObjectAsync('system.adapter.' + this._adapter.namespace, resultObject);
                return;
            } else {
                this._adapter.log.error(`[asyncRestartAdapter()]: getForeignObjectAsync() No object provided from function.`);
                return;
            }

        } catch (error) {
            this._adapter.log.error(`[asyncRestartAdapter()]: ${error}`);
            return;
        }
        
    }
    */

    /** 
     * Check if a state exists 
     * @param {string}   str                State path
     * @param {boolean}  [isForeign=false]  true if a foreign state, false if a state of this adapter
     * @return {Promise<boolean>} 
     */
    async asyncStateExists (str, isForeign=false) {

        try {
            let ret;
            if (isForeign) {
                ret = await this._adapter.getForeignObjectAsync(str);
            } else {
                ret = await this._adapter.getObjectAsync(str);
            }
            
            if (ret == undefined) {
                return false;
            } else {
                return true;
            }
        } catch (error) {
            this.dumpError('[asyncStateExists()]', error);
            return false;
        }

    }

    /**
     * Checks if an ioBroker state id (path) string is valid. 
     * NOTE: This does not verify if the state is existing. It does just some basic checks if the path looks good-
     * 
     * @param {string}   str     String to validate
     * @return {boolean}         true/false of validation result
     */
    isStateIdValid(str) {

        // String?
        if (!str || typeof str !== 'string') return false;

        // If string length < 5 chars (minimum state length is 5, assuming this is a valid state: 'x.0.a'
        if(str.length < 5) return false;

        // If forbidden chars
        if (this._globals.forbiddenStatePaths.test(str)) return false;

        // If no instance number with a leading and trailing dot "."
        if (!/\.\d+\./.test(str)) return false;

        // If spaces at beginning or end
        if (str.startsWith(' ') || str.endsWith(' ')) return false;

        // If dots at beginning or end
        if (str.startsWith('.') || str.endsWith('.')) return false;

        // All passed
        return true;

    }


    /**
     * Create States, either under adapter instance or anywhere.
     * TODO: Better error handling (callback: function (err, _obj) ) for set(Foreign)ObjectNotExistsAsync and set(Foreign)StateAsync
     * 
     * @param {object|array}  statesToCreate    Object like {statePath:'0_userdata.0.motionHallway', commonObject:{name:'Motion', type:'boolean', read:true, write:true, role:'state', def:false }
     *                                          You can pass multiple objects via array of these objects.
     * @param {boolean}       [isForeign=false] False: creates states under adapter instance, if true: creates states anywhere.
     *                                          WARN: use the "true" flag carefully!
     * @return {Promise<boolean>}               false if errors caught, otherwise true.
     */
    async asyncCreateStates(statesToCreate, isForeign=false) {

        try {

            // Convert to array if it is an object, since we allow both
            if(!Array.isArray(statesToCreate)) statesToCreate = [statesToCreate];

            for (const lpStateToCreate of statesToCreate) {
                
                // Check parameters
                if (!lpStateToCreate.statePath || !lpStateToCreate.commonObject) {
                    this._adapter.log.error(`asyncCreateForeignStates(): invalid parameter provided to function`);
                    return false;
                }

                // Create new state. 
                // While set(Foreign)ObjectNotExistsAsync takes care if state does not exist, we still check as we also set values afterwards.
                if (isForeign && ! await this.asyncStateExists(lpStateToCreate.statePath, true)) {
                    await this._adapter.setForeignObjectNotExistsAsync(lpStateToCreate.statePath, {type:'state', common:lpStateToCreate.commonObject, native: {}});
                } else if(!isForeign && ! await this.asyncStateExists(lpStateToCreate.statePath, false)) {
                    await this._adapter.setObjectNotExistsAsync(lpStateToCreate.statePath, {type:'state', common:lpStateToCreate.commonObject, native: {}});
                } else {
                    // State already exists.
                    continue;
                }

                // Set default value
                const def = lpStateToCreate.commonObject.def;
                const type = lpStateToCreate.commonObject.type;
                let initialDefault;
                if (def !== undefined) {
                    initialDefault = def;
                } else {
                    if(type === 'number')  initialDefault = 0;
                    if(type === 'boolean') initialDefault = false;
                    if(type === 'string')  initialDefault = '';
                    if(type === 'array')   initialDefault = [];
                    if(type === 'object')  initialDefault = {};
                }       
                if (isForeign) {
                    await this._adapter.setForeignStateAsync(lpStateToCreate.statePath, initialDefault, false);
                } else {
                    await this._adapter.setStateAsync(lpStateToCreate.statePath, initialDefault, false);
                }                
                this._adapter.log.debug(`State '${lpStateToCreate.statePath}' created.`);

            }

            return true;
            

        } catch (error) {
            this.dumpError('[asyncCreateForeignStates()]', error);
            return true;
        }

    }


    /**
     * Get foreign state value
     * 
     * @param {string}      statePath  - Full path to state, like 0_userdata.0.other.isSummer
     * @return {Promise<*>}            - State value
     */
    async asyncGetForeignStateValue(statePath) {
        try {
            const stateObject = await this.asyncGetForeignState(statePath);
            if (stateObject == null) return null; // error thrown already in asyncGetForeignState()
            return stateObject.val;            
        } catch (error) {
            this.dumpError(`[asyncGetForeignStateValue]`, error);
            return null;
        }

    }    

    /**
     * Get foreign state
     * 
     * @param {string}      statePath  - Full path to state, like 0_userdata.0.other.isSummer
     * @return {Promise<object>}       - State object: {val: false, ack: true, ts: 1591117034451, …}, or null if error
     */
    async asyncGetForeignState(statePath) {

        try {
            // Check state existence
            const stateObject = await this._adapter.getForeignObjectAsync(statePath);

            if (!stateObject) {
                throw(`State '${statePath}' does not exist.`);
            } else {
                // Get state value, so like: {val: false, ack: true, ts: 1591117034451, …}
                const stateValueObject = await this._adapter.getForeignStateAsync(statePath);
                if (! this.isLikeEmpty(stateValueObject)) {
                    return stateValueObject;
                } else {
                    throw(`Unable to retrieve info from state '${statePath}'.`);
                }
            }
        } catch (error) {
            this.dumpError(`[asyncGetForeignState]`, error);
            return null;
        }

    }

    /**
     * Returns a value as configured in Administration Settings of ioBroker
     * @param {string}  what     Options like: city, country, longitude, latitude, language, tempUnit, 
     *                                         currency, dateFormat, isFloatComma, licenseConfirmed, 
     *                                         defaultHistory, activeRepo, diag
     *                           To see all options, use: log('All options: ' +  JSON.stringify(getObject('system.config').common));
     * @return {Promise<*>}      The option. Will be undefined if value is not set.
     */
    async asyncGetSystemConfig(what) {

        try {
            const config = await this._adapter.getForeignObjectAsync('system.config');
            if (this.isLikeEmpty(config.common[what])) {
                this._adapter.log.warn(`[asyncGetSystemConfig] System config for [${what}] not found.`);
                return;
            } else {
                return config.common[what];
            }
        } catch (error) {
            this.dumpError('[asyncGetSystemConfig()]', error);
            return;
        }

    }

    /**
     *     ██████  ███████ ███    ██ ███████ ██████  ██  ██████          ██ ███████ 
     *    ██       ██      ████   ██ ██      ██   ██ ██ ██               ██ ██      
     *    ██   ███ █████   ██ ██  ██ █████   ██████  ██ ██               ██ ███████ 
     *    ██    ██ ██      ██  ██ ██ ██      ██   ██ ██ ██          ██   ██      ██ 
     *     ██████  ███████ ██   ████ ███████ ██   ██ ██  ██████      █████  ███████ 
     *
     *   Generic JS functions - independent from adapter
     */

    /**
     * Checks if time of '08:30' is between '23:00' and '09:00'.
     * Source/inspired by: https://stackoverflow.com/a/24577309
     * 
     * @param {string}    start - start string, like: '23:00', '07:30', 9:15'
     * @param {string}    end   - end string, like: '08:15', '17:23'
     * @param {string}    check - to check, like: '17:25', '9:30'
     * @return {boolean}  true if in between, false if not
     * 
     */
    isTimeBetween (start, end, check) {


        const timeIsBetween = function(start, end, check) {
            return (start.hour <= end.hour) ? check.isGreaterThan(start) && !check.isGreaterThan(end)
                : (check.isGreaterThan(start) && check.isGreaterThan(end)) || (!check.isGreaterThan(start) && !check.isGreaterThan(end));    
        };
    
        function getTimeObj (timeString) {
            const t = timeString.split(':');
            const returnObject = {};
            returnObject.hour = parseInt(t[0]);
            returnObject.minutes = parseInt(t[1]);
            returnObject.isGreaterThan = function(other) { 
                return (returnObject.hour > other.hour) || (returnObject.hour === other.hour) && (returnObject.minutes > other.minutes);
            };
            return returnObject;
        }
    
        return timeIsBetween(getTimeObj(start), getTimeObj(end), getTimeObj(check));
    
    }



    /**
     * Converts a given timestamp into European time. E.g. '9:03pm and 45 seconds' -> '21:03:45'
     * @param   {object}   ts                 Timestamp
     * @param   {boolean}  [noSeconds=false]  if true, seconds will not be added. Result is '21:04' (rounded)
     * @return  {string}    Date in European date/time as String
     */
    timestampToTimeString(ts, noSeconds=false) {

        if(noSeconds) ts = this.roundTimeStampToNearestMinute(ts);
        const inputDate = new Date(ts);        
        
        if (noSeconds) {
            return this.zeroPad(inputDate.getHours(), 2) + ':' + this.zeroPad(inputDate.getMinutes(), 2);
        } else {
            return this.zeroPad(inputDate.getHours(), 2) + ':' + this.zeroPad(inputDate.getMinutes(), 2) + ':' + this.zeroPad(inputDate.getSeconds(), 2);
        }

    }


    /**
     * Rounds the given timestamp to the nearest minute
     * Inspired by https://github.com/date-fns/date-fns/blob/master/src/roundToNearestMinutes/index.js
     * 
     * @param {number}  ts   a timestamp
     * @return {number}      the resulting timestamp
     */
    roundTimeStampToNearestMinute(ts) {

        const date = new Date(ts);
        const minutes = date.getMinutes() + date.getSeconds() / 60;
        const roundedMinutes = Math.floor(minutes);
        const remainderMinutes = minutes % 1;
        const addedMinutes = Math.round(remainderMinutes);
        return new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            roundedMinutes + addedMinutes
        ).getTime();

    }



    /**
     * Gets the time left of 'const xxx = setTimeout()', where xxx is the timer id.
     * 
     * @author  Mic-M <https://github.com/Mic-M/>
     * @version 0.1 (30 June 2020)
     * 
     * @param {object} timerId - The timer object 'xxx' when executing a 'const xxx = setTimeout()'
     * @return {number}  The time remaining of the setTimeout(). Returns -1, if not or no longer running, never run, or object not even set.
     * 
     */
    getTimeoutTimeLeft(timerId) {

        try {

            // Return -1 if timer object is undefined/null or if timer is not running
            if (!timerId || timerId._destroyed) return -1;

            const dummyTimer = setTimeout(() => {}, 0);               // We need a dummy timer to get the current number of ms when the node.js session was started
            const currMsAfterNodeJsStart = dummyTimer['_idleStart'];  // _idleStart is the number of milliseconds since the node.js session was started.
            clearTimeout(dummyTimer);                                 // clear dummy timer, just in case.

            const elapsed   = currMsAfterNodeJsStart - timerId._idleStart;     // for how many milliseconds is timer already running?
            let   remaining = timerId._idleTimeout - elapsed; // remaining time in ms
            if (remaining < 0 ) remaining = -1;  // Just in case, should be already caught by timerObject._destroyed

            return remaining;

        } catch (error) {
            this.dumpError('[getTimeoutTimeLeft()]', error);
            return -1;
        }   

    }




    /**
     * Checks whether variable is number
     * isNumber ('123'); // true  
     * isNumber ('123abc'); // false  
     * isNumber (5); // true  
     * isNumber ('q345'); // false
     * isNumber(null); // false
     * isNumber(undefined); // false
     * isNumber(false); // false
     * isNumber('   '); // false
     * @source https://stackoverflow.com/questions/1303646/check-whether-variable-is-number-or-string-in-javascript
     * @param {any} n     Variable, die zu prüfen ist auf Zahl
     * @return {boolean}  true falls Zahl, false falls nicht.
     */
    isNumber(n) { 
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n); 
    }


    /**
     * Remove certain (forbidden) chars from a string
     * @param {string} str                  - The input string
     * @param {RegExp} regex                - Regex of chars to be removed, like /[\][*,;'"`<>\\?]/g
     * @param {boolean} [cleanSpaces=false] - Optional: trim and remove replace multiple spaces with single space
     * @return {array} Array with two elements: [0]: the resulting string, [1]: message if str was altered or empty string if no changes applied
     */
    strCharsRemove(str, regex, cleanSpaces=false) {
        let correctedMsg = '';
        let result = str;
        if (result.match(regex)) {
            result = result.replace(regex, '');
            correctedMsg += 'removed certain chars;';
        }
        if (cleanSpaces) {
            // remove multiple spaces
            if (result.match(/  +/g)) {
                result = result.replace(/ +/g, ' '); // remove multiple spaces
                correctedMsg += 'removed multiple spaces;';
            }
            // remove spaces from the start and end
            if (result.startsWith(' ') || result.endsWith(' ')) {
                result = result.trim();
                correctedMsg += 'remove spaces from start and/or end;';
            }
        }
        return [result, correctedMsg];
    }

    /**
     * Removing Array element(s) by input value. 
     * @param {array}   arr             the input array
     * @param {string}  valRemove       the value to be removed
     * @param {boolean} [exact=true]    OPTIONAL: default is true. if true, it must fully match. if false, it matches also if valRemove is part of element string
     * @return {array}  the array without the element(s)
     */
    arrayRemoveElementByValue(arr, valRemove, exact=true) {

        const arrGiven = [...arr]; // copy array

        for ( let i = 0; i < arrGiven.length; i++){ 
            if (exact) {
                if ( arrGiven[i] === valRemove) {
                    arrGiven.splice(i, 1);
                    i--;
                }
            } else {
                if (arrGiven[i].indexOf(valRemove) != -1) {
                    arrGiven.splice(i, 1);
                    i--;
                }
            }
        }
        return arrGiven;
    }


    /**
     * Remove duplicates from an array of objects by key
     * https://stackoverflow.com/a/58437069
     * 
     * @param {array} givenArray
     * @param {string}  key             key which value must be unique.
     * @param {boolean} [all=false]     if true: unique by all properties of the object, if false: unique by just the key value
     */
    uniqueArrayObjectByKey(givenArray, key, all=false) {

        if (all) {
            return givenArray.filter((v,i,a)=>a.findIndex(t=>(JSON.stringify(t) === JSON.stringify(v)))===i);
        } else {
            return givenArray.filter((v,i,a)=>a.findIndex(t=>(t[key] === v[key]))===i);
        }

    }    

    /**
     * Remove Duplicates from Array
     * @source https://stackoverflow.com/questions/23237704/nodejs-how-to-remove-duplicates-from-array
     * @param  {array} inputArray        Array to process
     * @return {array}                  Array without duplicates.
     */
    uniqueArray(inputArray) {
        return inputArray.filter(function(elem, pos) {
            return inputArray.indexOf(elem) == pos;
        });
    }


    /**
     * Check if an array contains duplicate values
     * https://stackoverflow.com/a/34192063
     * 
     * @param {*} myArray   - The given array
     * @return {boolean}    - true if it is unique, false otherwise.
     */
    isArrayUnique(myArray) {
        return myArray.length === new Set(myArray).size;
    }


    /**
     * Checks if Array or String is not undefined, null or empty.
     * Array, object, or string containing just white spaces or >'< or >"< or >[< or >]< is considered empty
     * 18-Jun-2020: added check for { and } to also catch empty objects.
     * 08-Sep-2019: added check for [ and ] to also catch arrays with empty strings.
     * @param  {any}  inputVar   Input Array or String, Number, etc.
     * @return {boolean} True if it is undefined/null/empty, false if it contains value(s)
     */
    isLikeEmpty(inputVar) {
        if (typeof inputVar !== 'undefined' && inputVar !== null) {
            let strTemp = JSON.stringify(inputVar);
            strTemp = strTemp.replace(/\s+/g, ''); // remove all white spaces
            strTemp = strTemp.replace(/"+/g, '');  // remove all >"<
            strTemp = strTemp.replace(/'+/g, '');  // remove all >'<
            strTemp = strTemp.replace(/\[+/g, '');  // remove all >[<
            strTemp = strTemp.replace(/\]+/g, '');  // remove all >]<
            strTemp = strTemp.replace(/\{+/g, '');  // remove all >{<
            strTemp = strTemp.replace(/\}+/g, '');  // remove all >}<
            if (strTemp !== '') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**

    * Pad a number with leading zeros, like 7 => '007' if 3 places.
    * @param  {string|number}  num     Input number
    * @param  {number}         digits  Number of digits
    * @return {string}         The result
    */
    zeroPad(num, digits) {
        const zero = digits - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join('0') + num;        
    } 

    /**
     * Check string against a forbidden chars regex
     * Mic-M
     * 
     * @param {string} str      - the input string
     * @param {RegExp} regex    - the regular expression, like: /[\][*,;'"`<>\\?]/g
     * @return {boolean|string} - returns false if nothing found, or all forbidden chars found space separated as string, like '[ ] , ? *'
     */
    forbiddenCharsInStr(str, regex) {

        let result = str.match(regex);
        if (result) {
            result = this.uniqueArray(result);
            let returnStr = '';
            for (const lpCharForbidden of result) {
                returnStr = returnStr + ' ' + lpCharForbidden;
            }
            return (returnStr.trim());
        } else {
            return false;
        }
    
    }




}



module.exports = Library;
