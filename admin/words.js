/*global systemDictionary:true */
/*eslint quotes: ["error", "double"]*/
"use strict";

systemDictionary = {
    "my-smart-control adapter settings": {           "en": "Adapter settings for my-smart-control",           "de": "Adaptereinstellungen für my-smart-control",       "ru": "Настройки адаптера для my-smart-control",         "pt": "Configurações do adaptador para my-smart-control", "nl": "Adapterinstellingen voor my-smart-control",       "fr": "Paramètres d'adaptateur pour my-smart-control",   "it": "Impostazioni dell'adattatore per my-smart-control", "es": "Ajustes del adaptador para my-smart-control",     "pl": "Ustawienia adaptera dla my-smart-control",        "zh-cn": "my-smart-control的适配器设置"},
    "Select": { "en": "Select", "de": "Auswählen", "ru": "Выбрать", "pt": "Selecione", "nl": "Selecteer", "fr": "Sélectionner", "it": "Selezionare", "es": "Seleccione", "pl": "Wybierz", "zh-cn": "选择" },
    "Ok": { "en": "Ok", "de": "OK", "ru": "Хорошо", "pt": "Está bem", "nl": "OK", "fr": "D'accord", "it": "Ok", "es": "Okay", "pl": "Dobrze", "zh-cn": "好"},
    "Cancel": {"en": "Cancel", "de": "Abbrechen", "ru": "Отмена", "pt": "Cancelar", "nl": "Annuleer", "fr": "Annuler", "it": "Annulla", "es": "Cancelar", "pl": "Anuluj", "zh-cn": "取消"},
};

